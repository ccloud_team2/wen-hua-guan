module.exports = function(grunt) {
    require('time-grunt')(grunt); //Grunt处理任务进度条提示

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            development: {
                options: {
                    // paths: ['less/zhidao']
                },
                files: {
                    'css/style.css': 'less/style.less'
                }
            }
        },
        watch: {
            options: {
                //开启 livereload
                livereload: true,
                //显示日志
                dateFormate: function(time) {
                    grunt.log.writeln('编译完成,用时' + time + 'ms ' + (new Date()).toString());
                    grunt.log.writeln('Wating for more changes...');
                }
            },
            less: {
                files: ['less/*.less', 'less/*/*.less'],
                tasks: ['less']
            }
        }
    });

    grunt.event.on('watch', function(action, filepath, target) {
        grunt.log.writeln(target + ': ' + '文件: ' + filepath + ' 变动状态: ' + action);
    });

    grunt.registerTask('default', ['less']);
    grunt.loadNpmTasks("grunt-contrib-less");
    grunt.loadNpmTasks("grunt-contrib-watch");
}