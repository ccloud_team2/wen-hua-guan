<?php $result = $view->result; ?>
<?php $item = $result[0]; ?>
<?php $node = $item->_field_data['nid']['entity']; ?>
<?php $file = current($item->field_field_image); ?>
<div class="text-center media-item">
	<?php print render($file['rendered']) ?>
	<h5><?php print $node->title; ?></h5>
</div>