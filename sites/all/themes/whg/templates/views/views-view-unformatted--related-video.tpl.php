<?php $result = $view->result; ?>
<?php $path = base_path().current_path(); ?>
<div class="slick-container">
	<div class="thumbnails carousel slider slider-nav">
	<?php foreach ($result as $item): ?>
		<?php $node = $item->_field_data['nid']['entity']; ?>
		<div class="text-center media-item admin-actions-wrapper">
			<?php print whg_node_quick_edit_button($node); ?>
			<a class="" href="<?php print $path.'/video/'.$node->nid ?>">
			<?php print whg_video_get_thumbnail($node, 'slider_thumbnail', true); ?>
			</a>
			<a class="title" href="<?php print $path.'/video/'.$node->nid ?>"><?php print $node->title; ?></a>
		</div>
	<?php endforeach ?>
	</div>
</div>