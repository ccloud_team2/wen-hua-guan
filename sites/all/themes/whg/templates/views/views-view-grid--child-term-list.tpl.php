<?php $result = $view->result; ?>
<?php $type = arg(1); ?>
<ul class="person-list list-unstyled row">
<?php foreach ($result as $i => $item): ?>
<?php $path = url('type/'.$type.'/channel/'.$item->tid); ?>
<li class="col-sm-6">	
    <div class="avatar col-sm-4">
		<a href="<?php print $path; ?>">
		<?php if (!empty($item->views_field_view_view)): ?>
			<?php print $item->views_field_view_view ?>
		<?php else: ?>
			<img src="http://placehold.it/180x240" alt="" class="img-responsive" />
		<?php endif ?>
		</a>
    </div>
    <div class="biography col-sm-8">
    	<h4><a href="<?php print $path; ?>"><?php print $item->taxonomy_term_data_name ?></a></h4>
    	<?php if (!empty($item->views_field_view_view_1)): ?>
    	<div class="content"><?php print $item->views_field_view_view_1 ?></div>
    	<?php endif ?>    	
        <div class="click2view"><a href="<?php print $path; ?>">了解更多</a></div>
    </div>
</li>
<?php endforeach ?>
</ul>