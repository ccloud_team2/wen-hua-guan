<?php $result = $view->result; ?>
<?php $path = base_path().current_path(); ?>
<div class="slick-container">
	<div class="thumbnails carousel slider slider-nav audios">
	<?php foreach ($result as $item): ?>
	    <?php $node = $item->_field_data['nid']['entity']; ?>
	    <?php $file = current($item->field_field_file); ?>
	    <div class="media-item admin-actions-wrapper">
			<?php print whg_node_quick_edit_button($node); ?>
	        <!-- <a class="file-icon" href="<?php print $path.'/audio/'.$node->nid ?>"><i class="glyphicon glyphicon-music"></i></a> -->
	        <a class="title" href="<?php print $path.'/audio/'.$node->nid ?>"><?php print $node->title; ?></a>
	    </div>
	<?php endforeach ?>
	</div>
</div>