<?php 
$args = explode('/', $_GET['q']);
array_pop($args);
$path = base_path().implode('/', $args);
$result = $view->result; 
?>

<div class="slick-container">
	<div class="thumbnails carousel slider audios">
	<?php foreach ($result as $item): ?>
	    <?php $node = $item->_field_data['nid']['entity']; ?>
	    <?php $file = current($item->field_field_file); ?>
	    <div class="media-item">
	        <a href="<?php print $path.'/'.$node->nid ?>"><?php print $node->title; ?></a>
	    </div>
	<?php endforeach ?>
	</div>
</div>