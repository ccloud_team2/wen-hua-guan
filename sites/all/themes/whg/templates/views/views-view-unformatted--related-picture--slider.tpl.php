<?php $result = $view->result; ?>
<?php $total = count($result); ?>

<div class="slick-container">
    <div id="player" class="slide-player slider slider-for">
        <?php foreach ($result as $i=>$item): ?>
            <?php 
            $file = current($item->field_field_image);
            $node = $item->_field_data['nid']['entity'];
            $content = node_view($node, 'teaser');
            $largeImgUrl = image_style_url('colorbox_large', $file['raw']['uri']);
            hide($content['links']);
            hide($content['field_image']);
            ?>
            <div class="node-picture text-center">
                <h3 class="picture-title"><?php print $node->title; ?> ( <span class="text-danger"><?php print ($i+1) ?></span>/<?php print $total ?> )</h3>
                <div class="picture">
                    <a class="colorbox" href="<?php print $largeImgUrl ?>" rel="large" title="<?php print $node->title ?>"> 
                        <?php print theme('image_lazy_style', array('path'=>$file['raw']['uri'], 'style'=>'slider_full', 'attributes'=> array('class'=>''))); ?>
                    </a>
                </div>
                <div class="content text-left inline-fields">
                    <?php print drupal_render_children($content) ?>
                </div>                
            </div>
        <?php endforeach ?>
    </div>

    <div id="thumbnails" class="slide-player thumbnails carousel slider slider-nav">
        <?php foreach ($result as $item): ?>
            <?php $node = $item->_field_data['nid']['entity']; ?>
            <?php $file = current($item->field_field_image); ?>
            <div class="text-center media-item admin-actions-wrapper">
                <?php print whg_node_quick_edit_button($node); ?>
                <?php print theme('image_lazy_style', array('path'=>$file['raw']['uri'], 'style'=>'slider_thumbnail', 'attributes'=> array('class'=>''))); ?>
                <h5><?php print $node->title; ?></h5>
            </div>
        <?php endforeach ?>
    </div>
</div>