<?php 
$context = whg_base_page_context();
$style_name = 'cover_portrait';
if (isset($context['channel'])) {
	if ($context['channel']->name == '乐器') {
		$style_name = 'cover_landscape';
	}
}
?>

<?php $item = $view->result[0]; ?>
<?php $file = current($item->field_field_image); ?>
<?php print theme('image_style', array('style_name'=> $style_name, 
'path'=>$file['raw']['uri'], 
'attributes' => array('class'=>array('img-responsive'))
)); ?>
