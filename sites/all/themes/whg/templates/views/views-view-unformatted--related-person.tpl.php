<?php $result = $view->result; ?>
<ul class="person-list list-unstyled row">
<?php foreach ($result as $i => $item): ?>
<?php $node = $item->_field_data['nid']['entity']; ?>
<?php $file = current($item->field_field_photo); ?>
<?php $path = url('node/'.$node->nid); ?>

<li class="col-sm-6 admin-actions-wrapper">
    <?php print whg_node_quick_edit_button($node); ?>
    <div class="avatar col-sm-4">
        <a href="<?php print $path ?>">
        <?php if ($file): ?>
        <?php print theme('image_style', array('style_name'=>'cover_portrait', 
        'path'=>$file['raw']['uri'], 
        'attributes' => array('class'=>array('img-responsive'))
        )); ?>
        <?php else: ?>
        <img src="http://placehold.it/180x240" alt="" class="img-responsive" />
        <?php endif ?>
		<span class="name"><?php print $node->title; ?></span>
        </a>
    </div>
    <div class="biography col-sm-8">
        <div class="content">
            <?php if (isset($node->field_basic_info[LANGUAGE_NONE][0]['safe_value'])): ?>
            <?php print mb_strimwidth($node->field_basic_info[LANGUAGE_NONE][0]['safe_value'], 0, 180, '…', 'utf8') ?>
            <?php endif; ?>
        </div>
        <div class="click2view"><a href="<?php print $path ?>">了解更多</a></div>
    </div>
</li>
<?php endforeach ?>
</ul>
<style>

</style>