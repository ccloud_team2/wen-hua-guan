<?php $result = $view->result; ?>
<?php $type = arg(1); ?>
<ul class="simple-list list-unstyled clearfix">
<?php foreach ($result as $i => $item): ?>
<?php $path = url('type/'.$type.'/channel/'.$item->tid); ?>
<li class="col-xs-6 col-sm-4 col-md-3">
    <div class="media-item text-center">
		<a href="<?php print $path; ?>">
		<?php if (!empty($item->views_field_view_view)): ?>
			<?php print $item->views_field_view_view ?>
		<?php else: ?>
			<img src="http://placehold.it/180x240" alt="" class="img-responsive" />
		<?php endif ?>
		</a>
        <h4><a href="<?php print $path; ?>"><?php print $item->taxonomy_term_data_name ?></a></h4>
    </div>
</li>
<?php endforeach ?>
</ul>