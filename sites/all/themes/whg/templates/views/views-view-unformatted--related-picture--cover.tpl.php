<?php $item = $view->result[0]; ?>
<?php $node = $item->_field_data['nid']['entity']; ?>
<?php $file = current($item->field_field_image); ?>

<div class="cover text-center">
	<a href="<?php print base_path().current_path(); ?>/picture">
		<?php print theme('image_style', array('style_name'=>'cover_portrait', 
		'path'=>$file['raw']['uri'], 
		'attributes' => array('class'=>array('img-responsive'))
		)); ?>
	</a>
	<a class="h5" href="<?php print base_path().current_path(); ?>/picture">查看图集</a>
</div>