<div class="node-audio col-xs-6 col-sm-4 col-md-3">
	<div class="text-center media-item admin-actions-wrapper audio">
		<?php print whg_node_quick_edit_button($node); ?>		
		<a class="title" href="<?php print url('resource/'.$node->nid) ?>"><?php print mb_strimwidth(strip_tags($title), 0, 17, '...') ?></a>
	</div>
</div>
