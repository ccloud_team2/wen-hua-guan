<div class="col-xs-6 col-sm-4 col-md-3">
	<div class="text-center media-item admin-actions-wrapper">
		<?php print whg_node_quick_edit_button($node); ?>
	
		<a class="img-wrapper" href="<?php print url('resource/'.$node->nid) ?>">
		<?php print whg_video_get_thumbnail($node, 'slider_thumbnail'); ?>
		</a>
		<a class="title" href="<?php print url('resource/'.$node->nid) ?>"><?php print mb_strimwidth($title, 0, 17, '...'); ?></a>
	</div>
</div>
