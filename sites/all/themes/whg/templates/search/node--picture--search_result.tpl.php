<div class="col-xs-6 col-sm-4 col-md-3 node-picture">
	<div class="text-center media-item admin-actions-wrapper">
		<?php print whg_node_quick_edit_button($node); ?>

		<a class="img-wrapper" href="<?php print url('resource/'.$node->nid) ?>">
		<img class="img-responsive" src="<?php print image_style_url('slider_thumbnail', $node->field_image['und'][0]['uri']); ?>" alt="image" />
		</a>
		<a class="title" href="<?php print url('resource/'.$node->nid) ?>"><?php print mb_strimwidth($title, 0, 17, '...'); ?></a>
	</div>
</div>
