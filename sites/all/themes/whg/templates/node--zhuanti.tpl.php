<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

<h3 class="title"><?php print $title; ?></h3>
<?php print render($title_suffix) ?>
<div class="show-block">
  <?php print render($content['field_video']) ?>
  <div class="introduction-block">
    <?php print render($content['body']) ?>
  </div>
</div>

</div>
