<?php

/**
 * @file
 * template.php
 */

function whg_theme(){
  return array(
    'video' => array(
      'render element' => 'element',
    ),
    'audio' => array(
      'render element' => 'element',
    ),
    'image_lazy_style' => array(
      'variables' => array(
        'path' => null,
        'style' => null,
        'attributes' => array(),
      ),
    ),
    'views_exposed_form_whg_search_page' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Implements hook_preprocess_page().
 */
function whg_preprocess_page (&$variables) {
  $variables['theme_dir'] = base_path().drupal_get_path('theme', 'whg');

  // $context = whg_base_page_context();
  if ($context = whg_base_page_context()) {
    $banners = variable_get('whg_banner', array());
    if($fid = $banners[$context['type']->tid]) {
      $banner_file = file_load($fid);
      // $view = render(file_view($banner_file));
      $variables['whg_banner'] = theme('image', array('path'=>$banner_file->uri, 'attributes'=>array('class'=>'img-responsive banner-image')));
    }
  }

  if ($variables['is_front']) {
    $variables['page']['content']['#theme_wrappers'] = array();
  }
}

/**
 * Implements hook_process_page().
 */
function whg_process_page (&$variables) {
  $variables['navbar_classes'] = 'navbar navbar-default';
  // if (!isset($variables['page']['sidebar_first']['whg_base_side_menu'])) {
    // $variables['navbar_classes'] .= ' menu-closed';
  // }
  // $variables['navbar_classes'] = implode(' ', $navbar_classes);
}

function whg_preprocess_node(&$variables) {
  $variables['theme_hook_suggestions'][] = 'node__'.$variables['view_mode'];
  $variables['theme_hook_suggestions'][] = 'node__'.$variables['type'].'__'.$variables['view_mode'];

  $add_media_player = FALSE;

  if ($variables['view_mode'] !== 'search_result') {
    switch ($variables['type']) {
      case 'zhuanti':
      case 'video':
        if (isset($variables['content']['field_video'])) {
          $variables['content']['field_video']['#theme'] = 'video';
          $add_media_player = TRUE;
        }
        break;

      case 'audio':
        if (isset($variables['content']['field_file'])) {
          $variables['content']['field_file']['#theme'] = 'audio';
          $add_media_player = TRUE;
        }
        break;
      
      default:
        break;
    }
  }

  if ($add_media_player) {
    $lib = libraries_get_path('mediaelement');
    drupal_add_css($lib.'/build/mediaelementplayer.min.css');
    drupal_add_css(drupal_get_path('theme','whg').'/css/skin/mediaelementplayer.css');
    drupal_add_js($lib.'/build/mediaelement-and-player.min.js');
    drupal_add_js("jQuery('video,audio').mediaelementplayer();", array('type'=>'inline', 'scope'=>'footer'));
  }

  if ($variables['type'] == 'person') {
    
  }

}

/**
 * Implements hook_preprocess_block().
 */
function whg_preprocess_block(&$variables) {
  $block = $variables['block'];
  // dpm($variables);
  if ($block->module == 'search' && $block->delta == 'form') {
    $variables['classes_array'][] = 'visible-md visible-lg';
  }
}

/**
 * Implements hook_preprocess_views_view().
 */
function whg_preprocess_views_view(&$variables) {
  // dpm($variables);
  $view = $variables['view'];
  $view_name = $variables['name'];
  $display_id = $variables['display_id'];
  $is_slider_view = FALSE;
  $add_thumbnails_js = false;
  $add_video_player_js = false;
  $add_picture_player_js = false;

  if ($display_id === 'slider' || $display_id === 'player'
    || ($view_name === 'person_related' && in_array($display_id, array('related_audio', 'related_audio__player', 'related_video', 'related_video__player', 'related_picture__slider')))) {
    $is_slider_view = TRUE;
  }

  if ($is_slider_view) {        
    $lib_carousel = libraries_get_path('slick');
    if ($lib_carousel) {
      drupal_add_js($lib_carousel.'/slick.min.js');
      drupal_add_css($lib_carousel.'/slick.css');
    }
  }

  $options = false;
  
  switch ($view_name) {
    case 'related_picture':
      // picture display page
      if ($display_id === 'slider') {
        $add_picture_player_js = true;
      }
      break;

    case 'related_video':
      if ($display_id == 'player') {
        $add_video_player_js = true;
      }
    case 'related_audio':
    case 'person_related':
      // thumbnails slider
      if ($is_slider_view) {
        $add_thumbnails_js = true;
      }

      if ($display_id == 'related_video__player') {
        $add_video_player_js = true;
      }
      if ($display_id == 'related_picture__slider') {
        $add_picture_player_js = true;
        $add_thumbnails_js = false;
      }

      break;
  }

  if ($add_picture_player_js) {
    $options['player'] = array(
      'lazyLoad' => "ondemand", // or "progressive"
      'audoplay' => false,
      'respondTo' => "window",
      'slidesToShow' => 1,
      'slidesToScroll' => 1,
      'arrows' => true,
      'fade' => true,
      'swipe' => true,
      'draggable' => false,
      'pauseOnHover' => true,
      'adaptiveHeight' => true,
      'asNavFor' => "#thumbnails",
    );
    $options['thumbnails'] = array(
      // 'centerMode' => true,
      'lazyLoad' => 'progressive',
      'slidesToShow' => 1,
      'slidesToScroll' => 1,
      'arrows' => true,
      'variableWidth' => true,
      'focusOnSelect' => true,
      'asNavFor' => "#player",
      'infinite' => false,
    );
    drupal_add_js('sites/all/libraries/colorbox/jquery.colorbox.js');
    drupal_add_css(path_to_theme().'/css/colorbox.css');

  }

  if ($add_thumbnails_js) {
    $options['thumbnails'] = array(
      'lazyLoad' => 'progressive',
      'slidesToShow' => 1,
      'slidesToScroll' => 1,
      'arrows' => true,
      'variableWidth' => true,
      'focusOnSelect' => false,
      'infinite' => false,
    );

    if ($add_video_player_js) {
      $options['thumbnails']['slidesToShow'] = 4;
      $options['thumbnails']['vertical'] = true;
      $options['thumbnails']['variableWidth'] = false;
      $options['thumbnails']['responsive'] = array(
        array(
          'breakpoint' => 768,
          'settings' => array(
            'vertical' => false,
            'slidesToShow' => 1,
          ),
        )
      );
    }
  }

  if ($options) {
    drupal_add_js(array('slick' => array('options' => $options)), array('type'=>'setting'));
  }

}

/**
 * Overrides theme_whg_main_menu().
 */
function whg_whg_main_menu($variables) {
  $links = $variables['links'];
  $op_types = $variables['opera_types'];

  $context = whg_base_page_context();

  $classes = 'has-children';
  if (!isset($context['type'])) {
    $classes .= ' expanded';
  }

  $output = "<ul class='menu nav navbar-nav' id='whg-main-menu'>";
  $menu = '<ul class="nav navbar-nav container"><li><a href="'.url('zhuanti').'">专题片库</a></li>
  <li><a href="'.url('search').'">资源库</a></li></ul>';

  $output .= "<li class='home $classes'><a href='".url('home')."' data-toggle='collapse' data-target='#whg-main-menu-child-0'>首页</a><div id='whg-main-menu-child-0' class='collapse'><div class='container'>$menu</div></div></li>";

  $n = 1;
  foreach ($op_types as $tid => $name) {

    // reindex array to assign css classname
    $items = array(); $i = 0; 
    foreach ($links as $cid => $link) {

      $link['href'] = "type/$tid/channel/$cid";

      $key = 'whg-menu-item-'.$i; 

      $items[$key] = $link;
      $i++;
    }

    $menu = theme('links', array(
      'links'      => $items,
      'attributes' => array(
        // 'class' => array('links','inline'),
        'id'    => 'whg-main-menu-child-'.$n,
        'class' => array('nav','navbar-nav', 'container'),
      ),
    ));

    $first_link = array_shift($items);
    $classes = 'has-children whg-menu-'.$n;

    $attributes = array(
      'href'   => url($first_link['href']),
      'data-toggle'   => 'collapse',
      'data-target'   => "#whg-main-menu-child-$n",
    );
    $in = '';

    if(isset($context['type']) && $context['type']->tid == $tid){
      $classes .= ' expanded';
      $attributes['aria-expanded'] = 'true';
      $in = 'in';
    }
    $btn_attributes = drupal_attributes($attributes);
    $output .= "<li class='$classes'><a $btn_attributes>$name</a><div id='whg-main-menu-child-$n' class='collapse $in'><div class='container'>$menu</div></div></li>";
    // $output .= "<li class='$classes'><a href='".url($first_link['href'])."' data-toggle='collapse' data-target='.whg-menu-1 > div'>$name</a><div><div class='container'>$menu</div></div></li>";

    $n++;
  }
  $output .= "</ul>";

  return $output;
}

/**
 * @theme_menu_tree()
 * @theme_menu_tree__whg_menu()
 */
function whg_menu_tree__whg_menu($variables) {
  return '<ul class="whg-side-menu nav list-unstyled">'.$variables['tree'].'</ul>';
}

/**
 * @theme_menu_link()
 * @theme_menu_link__whg_menu()
 */
function whg_menu_link__whg_menu($variables) {

  $element = $variables['element'];

  // Add class 'open' to item if children menu is active menu path.
  if ($element['#below']) {
    $active_menu = $_GET['q'];
    foreach (element_children($element['#below']) as $key) {
      if ($element['#below'][$key]['#href'] == $active_menu) {
        $element['#attributes']['class'][] = 'open';
        break;
      }
    }
  }

  return theme('menu_link', $element);
}

/**
 * @theme_menu_tree()
 * @theme_menu_tree__whg_menu()
 */
function whg_menu_tree__menu_zhuanti($variables) {
  return '<ul class="whg-side-menu nav list-unstyled">'.$variables['tree'].'</ul>';
}

/**
 * @theme_menu_link()
 * @theme_menu_link__whg_menu()
 */
function whg_menu_link__menu_zhuanti($variables) {

  $element = $variables['element'];

  // Add class 'open' to item if children menu is active menu path.
  if ($element['#below']) {
    $active_menu = $_GET['q'];
    foreach (element_children($element['#below']) as $key) {
      if ($element['#below'][$key]['#href'] == $active_menu) {
        $element['#attributes']['class'][] = 'open';
        break;
      }
    }

    $element['#localized_options']['attributes']['class'] = array();
    $element['#localized_options']['fragment'] = 'toggle';

    $element['#href'] = '';
  }

  return theme('menu_link', $element);
}


/**
 * Overrides theme_facetapi_title().
 */
function whg_facetapi_title($variables) {
  return $variables['title'];
}

/**
 * Preprocess function for theme_facetapi_link_inactive().
 */
function whg_preprocess_facetapi_link_inactive(&$vars){
  $vars['count'] = NULL;
}

function whg_views_exposed_form_whg_search_page(&$variables) {
  $form = $variables['form'];
  dpm($form);
}

/**
 * theme_audio().
 */
function whg_audio($variables) {
  $element = $variables['element'];
  $audio = $element['#items'][0];

  $attributes['width'] = '100%';
  $attributes['src'] = file_create_url($audio['uri']);
  // $attributes['data-mejsoptions'] = '{"features": ["playpause", "progress", "volume"]}';

  $output = '<div class="mediaelement-audio">';
  $output .= '<audio ' . drupal_attributes($attributes) . ' ></audio>';
  $output .= '</div>';
  return $output;
}

/**
 * theme_video().
 */
function whg_video($variables){
  $lib = base_path().libraries_get_path('mediaelement');

  $node = $variables['element']['#object'];
  $video_oss = $node->field_video_oss ? $node->field_video_oss['und'][0]['value'] : false;

  $video = $variables['element']['#items'][0];

  if ($video_oss) {
    $src = $video_oss;
  }
  else {
    if ($video['oss_enabled'] && !empty($video['oss_url'])) {
      $src = str_replace('public://', $video['oss_url'], $video['uri']);
    }
    else {
      $src = file_create_url($video['uri']);
    }
  }

  $thumbnail = file_create_url($video['thumbnailfile']->uri);

  $thumbnail_path = str_replace('public://', variable_get('file_public_path', conf_path() . '/files') . '/' , $video['thumbnailfile']->uri);
  $info = image_get_info($thumbnail_path);
  $width = $info['width']; $height = $info['height'];

  $attributes['poster'] = $thumbnail;
  $attributes['width'] = $width;
  $attributes['height'] = $height;
  $mejs_options = array(
    'videoWidth' => '100%',
    'videoHeight' => '100%',
    'features' => array('playpause', 'progress', 'duration', 'volume', 'fullscreen'),
    'customError' => '<a id="getflash" href="http://get.adobe.com/cn/flashplayer/" target="_blank"><i class="glyphicon glyphicon-hand-right"></i> 请安装Flash观看视频</a>',
    // mediaelement will get incorrect plugin path when drupal js aggregation is enabled, thus we set it manually.
    'pluginPath' => $lib.'/build/',
  );
  $attributes['data-mejsoptions'] = drupal_json_encode($mejs_options);
  if (isset($variables['attributes'])) {
    $attributes = $variables['attributes'] + $attributes;
  }
  if (isset($variables['element']['#attributes'])) {
    $attributes = $variables['element']['#attributes'] + $attributes;
  }


  $output = '<div class="mediaelement-video">'."\n";
  $output .= '<video ' . drupal_attributes($attributes) . ' >'."\n";
  // if ($video_oss) {
    $output .= '<source type="video/mp4" src="'.$src.'" />'."\n";
  // }
  // else {
  //   foreach ($video['playablefiles'] as $file) {
  //     $output .= '<source type="'.$file->filemime.'" src="'.file_create_url($file->uri).'" />'."\n";
  //   }
  // }
  
  $output .= '<img class="img-responsive" src="'.$thumbnail.'" alt="" width="'.$width.'" height="'.$height.'" />'."\n";
  $output .= 
    "<object width='100%' height='100%' type='application/x-shockwave-flash' data='$lib/build/flashmediaelement.swf'>
        <param name='movie' value='$lib/build/flashmediaelement.swf' />
        <param name='flashvars' value='controls=true&file=$src' />
        <!-- Image as a last resort -->
        <img src='$thumbnail' alt='' width='$width' height='$height' class='img-responsive' />
    </object>\n";
  $output .= '</video>';
  $output .= '</div>';
  return $output;
}

/**
 * @theme_image()
 */
// function whg_image($variables) {
//   $info = image_get_info($variables['path']);
//   // $variables['width'] = $info['width'];
//   // $variables['height'] = $info['height'];
//   // dpm($variables);
//   return theme_image($variables);
// }

/**
 * @theme_image_lazy_style().
 */
function whg_image_lazy_style($variables) {
  $uri = image_style_path($variables['style'], $variables['path']);
  $info = image_get_info($uri);
  $variables['attributes']['width'] = $info['width'];
  $variables['attributes']['height'] = $info['height'];
  $variables['attributes']['class'][] = 'lazyload';

  // image_style_path won't add cache token.
  $url = image_style_url($variables['style'], $variables['path']);
  $variables['attributes']['data-lazy'] = $url;

  $attributes = drupal_attributes($variables['attributes']);
  return "<img $attributes>";
}


/**
 * Get thumbnail for a video node.
 * @param  $node        A video node.
 * @param  $image_style Image style name for the thumbnail.
 * @param  $lazyload    theme as a lazyload image.
 */
function whg_video_get_thumbnail($node, $image_style = NULL, $lazyload = false) {
  $video = field_get_items('node', $node, 'field_video');
  if (isset($video[0])) {
    $file_uri = $video[0]['thumbnailfile']->uri;
    if ($image_style) {
      if ($lazyload) {
        $image = theme('image_lazy_style', array('path'=>$file_uri, 'style'=>$image_style));
      }
      else {
        $image = theme('image_style', array('path'=>$file_uri, 'style_name'=>$image_style, 'attributes' => array('class'=> 'img-responsive')));
      }
    }
    else {
      $image = theme('image', array('path'=>$file_uri, 'attributes' => array('class'=> 'img-responsive')));
    }    
  }
  else {
    // @todo: set default thumbnail
    $image = '<img src="http://placehold.it/160x120" class="img-responsive" />';
  }

  return $image;
}


function whg_node_quick_edit_button($node) {
  if ($node) {
    if (node_access('update', $node)) {
      $link = l('编辑', 'node/'.$node->nid.'/edit', array(
        'query' => drupal_get_destination(),
        'attributes' => array(
          'class'=>array('btn','btn-xs','btn-warning')
        ),
      ));
      return "<div class='admin-actions'>$link</div>";     
    }
  }
}

