/*!
 * hoverIntent v1.8.0 // 2014.06.29 // jQuery v1.9.1+
 * http://cherne.net/brian/resources/jquery.hoverIntent.html
 */
(function($){$.fn.hoverIntent=function(handlerIn,handlerOut,selector){var cfg={interval:100,sensitivity:6,timeout:0};if(typeof handlerIn==="object"){cfg=$.extend(cfg,handlerIn)}else{if($.isFunction(handlerOut)){cfg=$.extend(cfg,{over:handlerIn,out:handlerOut,selector:selector})}else{cfg=$.extend(cfg,{over:handlerIn,out:handlerIn,selector:handlerOut})}}var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if(Math.sqrt((pX-cX)*(pX-cX)+(pY-cY)*(pY-cY))<cfg.sensitivity){$(ob).off("mousemove.hoverIntent",track);ob.hoverIntent_s=true;return cfg.over.apply(ob,[ev])}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=false;return cfg.out.apply(ob,[ev])};var handleHover=function(e){var ev=$.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t)}if(e.type==="mouseenter"){pX=ev.pageX;pY=ev.pageY;$(ob).on("mousemove.hoverIntent",track);if(!ob.hoverIntent_s){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}}else{$(ob).off("mousemove.hoverIntent",track);if(ob.hoverIntent_s){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob)},cfg.timeout)}}};return this.on({"mouseenter.hoverIntent":handleHover,"mouseleave.hoverIntent":handleHover},cfg.selector)}})(jQuery);

(function($){

	$(function(){

		$('img[data-lazy]').load(function(){
			$(this).addClass('lazy-processed').removeClass('lazyload');
		});
		// $( ".intro-block" ).each( function(){
  //           $( this ).children( ".content" ).hover(
  //               function(){
  //                   $( this ).parent().siblings().addClass( "unactive" );
  //               },
  //               function(){
  //                   $( this ).parent().siblings().removeClass( "unactive" );
  //               }) ;
  //       });

		if (Drupal.settings.slick) {
			if (Drupal.settings.slick.options.player) {
				$('#player').slick(Drupal.settings.slick.options.player);

				$('.colorbox').colorbox({
					transition: 'none',
					fadeOut: 0,
					opacity: 0.8,
					width: '100%',
					height: '100%',
					maxHeight: '90%',
					current: "{current}/{total}",
					imgError: "图片加载失败",
					fixed: true
				});
			};
			if (Drupal.settings.slick.options.thumbnails) {
				$('.carousel').slick(Drupal.settings.slick.options.thumbnails);
			};
		};

		$('video').bind('contextmenu', function(){
			return false;
		});

		$('.aside-left-toggle').click(function(){
			$('body').toggleClass('aside-left-open');
		});
		
		var $mainMenu = $('#whg-main-menu');

		$mainMenu.on('click', 'a[data-toggle]', function(ev){
			if(window.matchMedia) {
				// mobile devices.
				if (matchMedia('screen and (max-width:767px)').matches) {
					// disable jumping.
					ev.preventDefault();
					// group behaviors.
					var actives = $mainMenu.find('.in');
					if (actives && actives.length) {
						actives.collapse('hide');
					};
				}
				else {
					ev.stopPropagation();
				}
			}
			else {
				ev.stopPropagation();
			}
		});

		$mainMenu.hoverIntent({
			over: function(){
				$(this).siblings().removeClass('expanded').end().addClass('expanded');
			},
			selector: 'li.has-children'
		});
	});

})(jQuery);