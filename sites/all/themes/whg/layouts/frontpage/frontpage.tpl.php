<div class="region" id="zhuanti-block">
	<div class="block">
		<div class="row">
			<div class="text col-md-6 col-md-push-6">
				<?php print $content['zhuanti_menu'] ?>
			</div>
			<div class="video col-md-6 col-md-pull-6">
				<?php print $content['zhuanti_video'] ?>
			</div>
		</div>
	</div>
</div>
<div class="region">
	<div class="row">
		<div class="block col-md-3 intro-block intro-block-1">
			<?php print $content['zhengzi_intro'] ?>
		</div>
		<div class="block col-md-3 intro-block intro-block-2">
			<?php print $content['xiqin_intro'] ?>
		</div>
		<!-- <div class="hidden-md hidden-lg clearfix"></div> -->
		<div class="block col-md-3 intro-block intro-block-3">
			<?php print $content['baizi_intro'] ?>
		</div>
		<div class="block col-md-3 intro-block intro-block-4">
			<?php print $content['huachao_intro'] ?>
		</div>
	</div>
</div>