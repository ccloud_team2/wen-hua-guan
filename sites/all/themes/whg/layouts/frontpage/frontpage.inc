<?php

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('front page'),
  'category' => t('文化馆非遗'),
  'icon' => 'frontpage.png',
  'theme' => 'frontpage',
  'regions' => array(
  	'zhuanti_video' => t('专题片视频'),
  	'zhuanti_menu' => t('专题片链接'),
  	'zhengzi_intro' => t('正字戏简介'),
  	'xiqin_intro' => t('西秦戏简介'),
  	'baizi_intro' => t('白字戏简介'),
  	'huachao_intro' => t('花朝戏简介'),
  ),
);