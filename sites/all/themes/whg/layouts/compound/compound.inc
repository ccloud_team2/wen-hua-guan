<?php

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('Compound'),
  'category' => t('文化馆非遗'),
  'icon' => 'compound.png',
  'theme' => 'compound',
  // 'css' => 'compound.css',
  'regions' => array(
  	'text' => t('Articles'),
  	'image' => t('Images'),
  	'audio' => t('Audios'),
  	'video' => t('Videos'),
  ),
);