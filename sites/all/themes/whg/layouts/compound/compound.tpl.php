<?php
/**
 * @file
 * Template for a 3 column panel layout.
 *
 * This template provides a very simple "one column" panel display layout.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   $content['middle']: The only panel in the layout.
 */

$article_class = !empty($content['image']) ? 'col-md-9' : 'col-md-12';
?>
<?php if ($content['text']): ?>
    <div class="whg-articles <?php print $article_class ?>"><?php print $content['text']; ?></div>
<?php endif ?>
<?php if ($content['image']): ?>
    <div class="whg-images col-md-3"><?php print $content['image']; ?></div>
<?php endif ?>
<?php if ($content['audio']): ?>
    <div class="whg-audios col-md-12">
        <?php print $content['audio']; ?>
    </div>
<?php endif ?>
<?php if ($content['video']): ?>
    <div class="whg-videos col-md-12">
        <?php print $content['video']; ?>
    </div>
<?php endif ?>
