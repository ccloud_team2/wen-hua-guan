<?php

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('List'),
  'category' => t('文化馆非遗'),
  'icon' => 'list.png',
  'theme' => 'list',
  'regions' => array(
  	'content' => t('Content'),
  ),
);