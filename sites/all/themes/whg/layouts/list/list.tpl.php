<?php
/**
 * @file
 * Template for a 3 column panel layout.
 *
 * This template provides a very simple "one column" panel display layout.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   $content['middle']: The only panel in the layout.
 */

$article_class = !empty($content['image']) ? 'col-md-8' : 'col-md-12';
?>
<div class="col-md-12">
	<?php print $content['content']; ?>
</div>
