<?php

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('Video Player'),
  'category' => t('文化馆非遗'),
  'icon' => 'video_player.png',
  'theme' => 'video_player',
  'regions' => array(
  	'video' => t('Video'),
  	'list' => t('List'),
  ),
);