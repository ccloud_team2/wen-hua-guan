<div class="region" id="zhuanti-block">
	<div class="block">
		<div class="row">
			<div class="text col-md-6 col-md-push-6">
				<div class="row">
					<div class="col-xs-12 col-sm-8 vertical-text-float zhuanti-intro">
						<h4>稀有剧目整体介绍</h4>
						<p>白字、正字、西秦、花朝等</p>
						<p>稀有剧种目前都只有一个专</p>
						<p>业剧团。因此，它们被称为</p>
						<p>「天下第一团」，指的就是</p>
						<p>这种因经济困难而独自支撑</p>
						<p>的境况。不过，它们都是有</p>
						<p>古老的历史和辉煌的时期。</p>
					</div>
					<div class="col-sm-4">
						<ul class="zhuanti-list list-unstyled">
							<li><a href="#">正字戏专题片</a></li>
							<li><a href="#">白字戏专题片</a></li>
							<li><a href="#">西秦戏专题片</a></li>
							<li><a href="#">花朝戏专题片</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="video col-md-6 col-md-pull-6">
				<img src="<?php print $theme_dir ?>/demo/front/video.png" alt="" class="img-responsive" />
			</div>
		</div>
	</div>
</div>
<div class="region">
	<div class="row">
		<div class="block col-md-3 col-sm-6 col-xs-6 intro-block intro-block-1">
			<div class="content">
				<div class="field-image">
					<img src="<?php print $theme_dir ?>/demo/front/b1.jpg" alt="" class="img-responsive" />
				</div>
				<div class="field-body">
					<strong>正字戏</strong>
					正字戏又称正音戏，因其语言用官话而得名。它是一个包括正音曲（杂以四平、青阳）以及昆腔、杂曲、小调等多种音腔的古老汉族戏曲剧种之一，正音曲为主要声腔。
				</div>				
			</div>
		</div>
		<div class="block col-md-3 col-sm-6 col-xs-6 intro-block intro-block-2">
			<div class="content">
				<div class="field-image">
					<img src="<?php print $theme_dir ?>/demo/front/b2.jpg" alt="" class="img-responsive" />
				</div>
				<div class="field-body">
					<strong>正字戏</strong>
					正字戏又称正音戏，因其语言用官话而得名。它是一个包括正音曲（杂以四平、青阳）以及昆腔、杂曲、小调等多种音腔的古老汉族戏曲剧种之一，正音曲为主要声腔。
				</div>				
			</div>
		</div>
		<div class="block col-md-3 col-sm-6 col-xs-6 intro-block intro-block-3">
			<div class="content">
				<div class="field-image">
					<img src="<?php print $theme_dir ?>/demo/front/b3.jpg" alt="" class="img-responsive" />
				</div>
				<div class="field-body">
					<strong>正字戏</strong>
					正字戏又称正音戏，因其语言用官话而得名。它是一个包括正音曲（杂以四平、青阳）以及昆腔、杂曲、小调等多种音腔的古老汉族戏曲剧种之一，正音曲为主要声腔。
				</div>				
			</div>
		</div>
		<div class="block col-md-3 col-sm-6 col-xs-6 intro-block intro-block-4">
			<div class="content">
				<div class="field-image">
					<img src="<?php print $theme_dir ?>/demo/front/b4.jpg" alt="" class="img-responsive" />
				</div>
				<div class="field-body">
					<strong>正字戏</strong>
					正字戏又称正音戏，因其语言用官话而得名。它是一个包括正音曲（杂以四平、青阳）以及昆腔、杂曲、小调等多种音腔的古老汉族戏曲剧种之一，正音曲为主要声腔。
				</div>				
			</div>
		</div>
	</div>
</div>