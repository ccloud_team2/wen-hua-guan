<?php
/**
 * Created by PhpStorm.
 * User: james
 * Date: 14-11-6
 * Time: 10:51
 */

function whg_base_admin_overview(){
  return '';
}

function whg_base_admin_menu_debug($pid = 0) {
  $vid = variable_get('whg_menu_vocabulary', 0);
  if($vid) {
    $links = whg_base_menu_data();
    $tree = whg_base_menu_tree_build($links);
    dpm($tree, 'tree build');

    $html = render($tree);
    return $html;
  }

  return '';
}

/**
 * Callback
 */
function whg_base_admin_basic_settings($form, &$form_state){
  $form = array();

  $op_vocab = variable_get('whg_opera_vocabulary', false);

  $options[0] = t('-- Select One --');
  $vocabs = taxonomy_get_vocabularies();
  foreach ($vocabs as $key => $vocab) {
    $options[$key] = $vocab->name;
  }

  $form['whg_opera_vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Opera Vocabulary'),
    '#description' => t('Select which vocabulary is used for opera types.'),
    '#options' => $options,
    '#default_value' => variable_get('whg_opera_vocabulary', 0),
    '#weight' => 0.009,
  );

  $form['whg_menu_vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Menu source'),
    '#description' => t('Select which vocabulary to be used as the menu source.'),
    '#options' => $options,
    '#default_value' => variable_get('whg_menu_vocabulary', 0),
    '#weight' => 0.010,
  );

  $form['whg_special_items_category'] = array(
    '#type' => 'textfield',
    '#title' => t('Special Items Term IDs'),
    '#description' => t('Special terms that should use list template, enter their ids separated with comma.'),
    '#default_value' => variable_get('whg_special_items_category', ''),
    '#weight' => 0.011,
  );


  $form['whg_banner'] = array(
    '#type' => 'fieldset',
    '#title' => t('Banner images'),
    '#description' => t('Upload banner images for each opera type.'),
    '#tree' => true,
    '#weight' => 10,
  );

  if($op_vocab) {
    $types = whg_base_get_opera_types();
    $banners = variable_get('whg_banner', array());

    foreach ($types as $id => $name) {
      $form['whg_banner'][$id] = array(
        '#type' => 'managed_file',
        '#name' => 'whg_banner_'.$id,
        '#title' => $name,
        '#default_value' => isset($banners[$id]) ? $banners[$id] : '',
        '#upload_location' => 'public://banners/',
      );
    }

    $form['#submit'][] = 'whg_base_admin_basic_submit';
  }
  else {
    $form['whg_banner']['warning'] = array(
      '#markup' => '<div class="messages warning">Please choose an opera vocabulary first.</div>'
    );
  }

  return system_settings_form($form);
}

function whg_base_admin_basic_submit($form, &$form_state) {

  foreach (element_children($form['whg_banner']) as $key) {
    if ($form['whg_banner'][$key]['#file']) {
      $file = file_load($form['whg_banner'][$key]['#file']->fid);
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
    }
  }

}

function whg_base_admin_oss($form, &$form_state) {

  $form['whg_video_oss_enabled'] = array(
    '#title' => '启用视频OSS',
    '#type' => 'checkbox',
    '#default_value' => variable_get('whg_video_oss_enabled', false),
  );

  $form['whg_video_oss_url'] = array(
    '#title' => 'OSS域名',
    '#description' => '只针对视频启用，因此填入域名需停止至视频文件夹。如 www.abc.com/a/b/vidoes/1.mp4, 则填写 www.abc.com/a/b',
    '#type' => 'textfield',
    '#default_value' => variable_get('whg_video_oss_url', ''),
  );

  return system_settings_form($form);
  
}
