/**
 * Created by james on 14-12-2.
 */
;
(function ($) {

    Drupal.behaviors.whg = {
        attach: function () {
            this.init();
            this.bindEvents();
        },
        init: function () {
            $.each(Drupal.settings.whg.node_form.filters, function (id, items) {
                $.each(items, function (i, tid) {
                    $('input[value="' + tid + '"]').parent().addClass('type-children type-' + id);
                });
            });
            $('ul li li .parent-term').parent().prev().addClass('clearfix');
        },
        bindEvents: function () {
            var typeWidget = $('#edit-field-tr-resource-category'),
                termTree = $('#edit-field-tr-menu-item');

            typeWidget.delegate('.form-checkbox', 'click', function () {
                var self = this;
                if (self.checked) {
                    termTree.addClass('active-' + self.value);
                }
                else {
                    termTree.removeClass('active-' + self.value);
                }
            });
        }
    };

})(jQuery);