<?php
/**
 * @file
 * whg_image_styles.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function whg_image_styles_image_default_styles() {
  $styles = array();

  // Exported image style: cover_landscape.
  $styles['cover_landscape'] = array(
    'name' => 'cover_landscape',
    'label' => 'Smart Cover Landscape (280x210)',
    'effects' => array(
      4 => array(
        'label' => 'Focus Scale And Crop',
        'help' => 'Scale and crop based on data provided by <em>ImageField Focus</em>.',
        'effect callback' => 'imagefield_focus_scale_and_crop_effect',
        'form callback' => 'imagefield_focus_scale_and_crop_form',
        'summary theme' => 'imagefield_focus_scale_and_crop_summary',
        'module' => 'imagefield_focus',
        'name' => 'imagefield_focus_scale_and_crop',
        'data' => array(
          'width' => 280,
          'height' => 210,
          'strength' => 'high',
          'fallback' => 'smartcrop',
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: cover_portrait.
  $styles['cover_portrait'] = array(
    'name' => 'cover_portrait',
    'label' => 'Smart Cover Portrait (300x400)',
    'effects' => array(
      2 => array(
        'label' => 'Focus Scale And Crop',
        'help' => 'Scale and crop based on data provided by <em>ImageField Focus</em>.',
        'effect callback' => 'imagefield_focus_scale_and_crop_effect',
        'form callback' => 'imagefield_focus_scale_and_crop_form',
        'summary theme' => 'imagefield_focus_scale_and_crop_summary',
        'module' => 'imagefield_focus',
        'name' => 'imagefield_focus_scale_and_crop',
        'data' => array(
          'width' => 300,
          'height' => 400,
          'strength' => 'high',
          'fallback' => 'smartcrop',
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: slider_full.
  $styles['slider_full'] = array(
    'name' => 'slider_full',
    'label' => 'Slider Full (scale - 810x540)',
    'effects' => array(
      3 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 810,
          'height' => 540,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: slider_thumbnail.
  $styles['slider_thumbnail'] = array(
    'name' => 'slider_thumbnail',
    'label' => 'Slider Thumbnail (180x120)',
    'effects' => array(
      5 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 180,
          'height' => 120,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
