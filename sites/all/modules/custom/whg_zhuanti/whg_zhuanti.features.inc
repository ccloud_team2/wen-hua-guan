<?php
/**
 * @file
 * whg_zhuanti.features.inc
 */

/**
 * Implements hook_node_info().
 */
function whg_zhuanti_node_info() {
  $items = array(
    'zhuanti' => array(
      'name' => t('专题片'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
