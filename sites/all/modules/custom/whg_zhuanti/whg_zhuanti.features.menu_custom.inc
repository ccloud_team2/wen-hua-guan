<?php
/**
 * @file
 * whg_zhuanti.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function whg_zhuanti_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-zhuanti.
  $menus['menu-zhuanti'] = array(
    'menu_name' => 'menu-zhuanti',
    'title' => '专题片',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('专题片');


  return $menus;
}
