<?php
/**
 * @file
 * whg_zhuanti.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function whg_zhuanti_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-zhuanti-body'
  $field_instances['node-zhuanti-body'] = array(
    'bundle' => 'zhuanti',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => TRUE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-zhuanti-field_video'
  $field_instances['node-zhuanti-field_video'] = array(
    'bundle' => 'zhuanti',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'video',
        'settings' => array(
          'poster_image_style' => '',
          'widthxheight' => '640x360',
        ),
        'type' => 'video_formatter_player',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_video',
    'label' => '视频',
    'required' => 0,
    'settings' => array(
      'default_dimensions' => '640x360',
      'file_directory' => 'videos',
      'file_extensions' => 'mp4 ogg avi mov wmv flv ogv webm',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'video',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'video_upload',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');
  t('视频');

  return $field_instances;
}
