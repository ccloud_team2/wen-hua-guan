<?php
/**
 * Created by PhpStorm.
 * User: james
 * Date: 14-12-23
 * Time: 17:10
 */

class WhgSearchApiDbService extends SearchApiDbService {

  protected function postQuery(array &$results, SearchApiQueryInterface $query) {
    if ($results['result count'] == 0) {
      if ($query->getOption('search_api_facets')) {
        $index = $query->getIndex();
        if (empty($this->options['indexes'][$index->machine_name])) {
          throw new SearchApiException(t('Unknown index @id.', array('@id' => $index->machine_name)));
        }
        $fields = $this->getFieldInfo($index);
        $db_query = $this->createDbQuery($query, $fields);

        $results['search_api_facets'] = $this->getFacets($query, clone $db_query);
      }
    }

  }

} 