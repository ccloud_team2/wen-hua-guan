<?php
/**
 * @file
 * site_admin.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function site_admin_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-add-content_:node/add/article
  $menu_links['menu-add-content_:node/add/article'] = array(
    'menu_name' => 'menu-add-content',
    'link_path' => 'node/add/article',
    'router_path' => 'node/add/article',
    'link_title' => '文章',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-add-content_:node/add/article',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-add-content_:node/add/audio
  $menu_links['menu-add-content_:node/add/audio'] = array(
    'menu_name' => 'menu-add-content',
    'link_path' => 'node/add/audio',
    'router_path' => 'node/add/audio',
    'link_title' => '音频',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-add-content_:node/add/audio',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-add-content_:node/add/person
  $menu_links['menu-add-content_:node/add/person'] = array(
    'menu_name' => 'menu-add-content',
    'link_path' => 'node/add/person',
    'router_path' => 'node/add/person',
    'link_title' => '人物',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-add-content_:node/add/person',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-add-content_:node/add/picture
  $menu_links['menu-add-content_:node/add/picture'] = array(
    'menu_name' => 'menu-add-content',
    'link_path' => 'node/add/picture',
    'router_path' => 'node/add/picture',
    'link_title' => '图片',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-add-content_:node/add/picture',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-add-content_:node/add/video
  $menu_links['menu-add-content_:node/add/video'] = array(
    'menu_name' => 'menu-add-content',
    'link_path' => 'node/add/video',
    'router_path' => 'node/add/video',
    'link_title' => '视频',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-add-content_:node/add/video',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-data-dict_---:admin/structure/taxonomy/liability_manner_article
  $menu_links['menu-data-dict_---:admin/structure/taxonomy/liability_manner_article'] = array(
    'menu_name' => 'menu-data-dict',
    'link_path' => 'admin/structure/taxonomy/liability_manner_article',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => '责任方式 - 文章',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-data-dict_---:admin/structure/taxonomy/liability_manner_article',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: menu-data-dict_---:admin/structure/taxonomy/liability_manner_audio
  $menu_links['menu-data-dict_---:admin/structure/taxonomy/liability_manner_audio'] = array(
    'menu_name' => 'menu-data-dict',
    'link_path' => 'admin/structure/taxonomy/liability_manner_audio',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => '责任方式 - 音频',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-data-dict_---:admin/structure/taxonomy/liability_manner_audio',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -43,
    'customized' => 1,
  );
  // Exported menu link: menu-data-dict_---:admin/structure/taxonomy/liability_manner_picture
  $menu_links['menu-data-dict_---:admin/structure/taxonomy/liability_manner_picture'] = array(
    'menu_name' => 'menu-data-dict',
    'link_path' => 'admin/structure/taxonomy/liability_manner_picture',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => '责任方式 - 图片',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-data-dict_---:admin/structure/taxonomy/liability_manner_picture',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: menu-data-dict_---:admin/structure/taxonomy/liability_manner_video
  $menu_links['menu-data-dict_---:admin/structure/taxonomy/liability_manner_video'] = array(
    'menu_name' => 'menu-data-dict',
    'link_path' => 'admin/structure/taxonomy/liability_manner_video',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => '责任方式 - 视频',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-data-dict_---:admin/structure/taxonomy/liability_manner_video',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -42,
    'customized' => 1,
  );
  // Exported menu link: menu-data-dict_:admin/structure/taxonomy/duty_type
  $menu_links['menu-data-dict_:admin/structure/taxonomy/duty_type'] = array(
    'menu_name' => 'menu-data-dict',
    'link_path' => 'admin/structure/taxonomy/duty_type',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => '职责分类',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-data-dict_:admin/structure/taxonomy/duty_type',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-data-dict_:admin/structure/taxonomy/language
  $menu_links['menu-data-dict_:admin/structure/taxonomy/language'] = array(
    'menu_name' => 'menu-data-dict',
    'link_path' => 'admin/structure/taxonomy/language',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => '语种',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-data-dict_:admin/structure/taxonomy/language',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-data-dict_:admin/structure/taxonomy/menu_item
  $menu_links['menu-data-dict_:admin/structure/taxonomy/menu_item'] = array(
    'menu_name' => 'menu-data-dict',
    'link_path' => 'admin/structure/taxonomy/menu_item',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => '菜单项',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-data-dict_:admin/structure/taxonomy/menu_item',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-data-dict_:admin/structure/taxonomy/resource_category
  $menu_links['menu-data-dict_:admin/structure/taxonomy/resource_category'] = array(
    'menu_name' => 'menu-data-dict',
    'link_path' => 'admin/structure/taxonomy/resource_category',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => '内容类别',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-data-dict_:admin/structure/taxonomy/resource_category',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-data-dict_:admin/structure/taxonomy/tags
  $menu_links['menu-data-dict_:admin/structure/taxonomy/tags'] = array(
    'menu_name' => 'menu-data-dict',
    'link_path' => 'admin/structure/taxonomy/tags',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => '关键字',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-data-dict_:admin/structure/taxonomy/tags',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('人物');
  t('关键字');
  t('内容类别');
  t('图片');
  t('文章');
  t('职责分类');
  t('菜单项');
  t('视频');
  t('语种');
  t('责任方式 - 图片');
  t('责任方式 - 文章');
  t('责任方式 - 视频');
  t('责任方式 - 音频');
  t('音频');


  return $menu_links;
}
