<?php
/**
 * @file
 * site_admin.features.uuid_user.inc
 */

/**
 * Implements hook_uuid_features_default_users().
 */
function site_admin_uuid_features_default_users() {
  $users = array();

  $users[] = array(
  'name' => '网站管理员',
  'mail' => 'temp@createcloud.cn',
  'theme' => '',
  'signature' => '',
  'signature_format' => 'filtered_html',
  'created' => 1416207551,
  'access' => 1416271006,
  'login' => 1416219141,
  'status' => 1,
  'timezone' => 'Asia/Chongqing',
  'language' => 'zh-hans',
  'picture' => 0,
  'init' => 'temp@createcloud.cn',
  'data' => FALSE,
  'uuid' => '35f1b31a-fcdf-4f69-9726-11ffd3329d5d',
  'roles' => array(
    2 => 'authenticated user',
    5 => '网站管理员',
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:UserAccount',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'homepage' => array(
      'predicates' => array(
        0 => 'foaf:page',
      ),
      'type' => 'rel',
    ),
  ),
  'date' => '2014-11-17 14:59:11 +0800',
);
  return $users;
}
