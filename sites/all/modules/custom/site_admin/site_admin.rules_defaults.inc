<?php
/**
 * @file
 * site_admin.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function site_admin_default_rules_configuration() {
  $items = array();
  $items['rules_redirect_to_site_admin_page'] = entity_import('rules_config', '{ "rules_redirect_to_site_admin_page" : {
      "LABEL" : "Redirect to site admin page",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_login" : [] },
      "IF" : [
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : { "3" : "3", "5" : "5" } }
          }
        }
      ],
      "DO" : [ { "redirect" : { "url" : "admin\\/site_admin", "destination" : "1" } } ]
    }
  }');
  return $items;
}
