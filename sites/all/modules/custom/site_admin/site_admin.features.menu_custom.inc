<?php
/**
 * @file
 * site_admin.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function site_admin_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-add-content.
  $menus['menu-add-content'] = array(
    'menu_name' => 'menu-add-content',
    'title' => '添加内容',
    'description' => '',
  );
  // Exported menu: menu-data-dict.
  $menus['menu-data-dict'] = array(
    'menu_name' => 'menu-data-dict',
    'title' => '数据字典',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('数据字典');
  t('添加内容');


  return $menus;
}
