<?php
/**
 * @file
 * site_admin.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function site_admin_user_default_roles() {
  $roles = array();

  // Exported role: 网站管理员.
  $roles['网站管理员'] = array(
    'name' => '网站管理员',
    'weight' => 3,
  );

  return $roles;
}
