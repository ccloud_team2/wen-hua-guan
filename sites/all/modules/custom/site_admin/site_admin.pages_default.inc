<?php
/**
 * @file
 * site_admin.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function site_admin_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'add_user';
  $page->task = 'page';
  $page->admin_title = 'Add user';
  $page->admin_description = '';
  $page->path = 'admin/site_admin/user/add-user';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 5,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'action',
    'title' => '添加用户',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_add_user_http_response';
  $handler->task = 'page';
  $handler->subtask = 'add_user';
  $handler->handler = 'http_response';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'HTTP response code',
    'contexts' => array(),
    'relationships' => array(),
    'code' => '301',
    'destination' => 'admin/people/create?destination=site_admin/user',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['add_user'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'content_administration';
  $page->task = 'page';
  $page->admin_title = 'Content administration';
  $page->admin_description = '';
  $page->path = 'admin/site_admin/content';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 5,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'action',
    'title' => '内容管理',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_content_administration_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'content_administration';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '内容管理';
  $display->uuid = 'f748ea2f-1fa7-4d67-b854-dd873fc04611';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-28083ec7-5628-4313-8972-5b61fc2810c1';
    $pane->panel = 'center';
    $pane->type = 'views';
    $pane->subtype = 'content_administration';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '30',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '28083ec7-5628-4313-8972-5b61fc2810c1';
    $display->content['new-28083ec7-5628-4313-8972-5b61fc2810c1'] = $pane;
    $display->panels['center'][0] = 'new-28083ec7-5628-4313-8972-5b61fc2810c1';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-28083ec7-5628-4313-8972-5b61fc2810c1';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['content_administration'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'message_administration';
  $page->task = 'page';
  $page->admin_title = 'Message administration';
  $page->admin_description = '';
  $page->path = 'admin/site_admin/message';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'action',
    'title' => '读者留言管理',
    'name' => 'navigation',
    'weight' => '2',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_message_administration_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'message_administration';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '读者留言管理';
  $display->uuid = 'f748ea2f-1fa7-4d67-b854-dd873fc04611';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-1583b60f-fc4c-4c3a-add2-aab29d2dce43';
    $pane->panel = 'center';
    $pane->type = 'views';
    $pane->subtype = 'message';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '20',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1583b60f-fc4c-4c3a-add2-aab29d2dce43';
    $display->content['new-1583b60f-fc4c-4c3a-add2-aab29d2dce43'] = $pane;
    $display->panels['center'][0] = 'new-1583b60f-fc4c-4c3a-add2-aab29d2dce43';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['message_administration'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'site_administration';
  $page->task = 'page';
  $page->admin_title = 'Site administration';
  $page->admin_description = '';
  $page->path = 'admin/site_admin';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 5,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => '网站管理',
    'name' => 'user-menu',
    'weight' => '99',
    'parent' => array(
      'type' => 'normal',
      'title' => '网站管理',
      'name' => 'main-menu',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_site_administration_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'site_administration';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
          1 => 'center_right',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => '中',
        'width' => '70.00289634698237',
        'width_type' => '%',
        'parent' => 'main-row',
      ),
      'center_right' => array(
        'type' => 'region',
        'title' => 'Center right',
        'width' => '29.99710365301763',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => array(
        'corner_location' => 'pane',
      ),
      'bottom' => array(
        'region_accordion_id' => 'site_admin_bottom',
        'active' => '0',
        'collapsible' => 1,
        'animated' => 'none',
        'autoHeight' => '1',
        'heightStyle' => 'content',
        'header' => 'h2',
      ),
      'center_right' => array(
        'corner_location' => 'pane',
      ),
    ),
    'center' => array(
      'style' => 'rounded_corners',
    ),
    'center_right' => array(
      'style' => 'rounded_corners',
    ),
    'bottom' => array(
      'style' => 'accordion',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '19068ae3-62ec-42ba-97ae-d885c969b3cf';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-2d16dc3e-d9f5-4c1a-846b-73c63e33c19a';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-add-content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'pane_collapsed' => 0,
      ),
      'style' => 'accordion',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2d16dc3e-d9f5-4c1a-846b-73c63e33c19a';
    $display->content['new-2d16dc3e-d9f5-4c1a-846b-73c63e33c19a'] = $pane;
    $display->panels['center'][0] = 'new-2d16dc3e-d9f5-4c1a-846b-73c63e33c19a';
    $pane = new stdClass();
    $pane->pid = 'new-ee8fccbb-84c3-4fde-996c-24be8dd449fe';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-data-dict';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'ee8fccbb-84c3-4fde-996c-24be8dd449fe';
    $display->content['new-ee8fccbb-84c3-4fde-996c-24be8dd449fe'] = $pane;
    $display->panels['center'][1] = 'new-ee8fccbb-84c3-4fde-996c-24be8dd449fe';
    $pane = new stdClass();
    $pane->pid = 'new-2a18576a-ef93-41b2-b765-4b3b57774ff9';
    $pane->panel = 'center_right';
    $pane->type = 'block';
    $pane->subtype = 'user-new';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '最新用户',
    );
    $pane->cache = array();
    $pane->style = array(
      'style' => 'accordion',
      'settings' => array(
        'pane_collapsed' => 1,
      ),
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2a18576a-ef93-41b2-b765-4b3b57774ff9';
    $display->content['new-2a18576a-ef93-41b2-b765-4b3b57774ff9'] = $pane;
    $display->panels['center_right'][0] = 'new-2a18576a-ef93-41b2-b765-4b3b57774ff9';
    $pane = new stdClass();
    $pane->pid = 'new-77f7e9f8-0bb3-41ad-b611-10adba2ff547';
    $pane->panel = 'center_right';
    $pane->type = 'block';
    $pane->subtype = 'user-online';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'pane_collapsed' => 1,
      ),
      'style' => 'accordion',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '77f7e9f8-0bb3-41ad-b611-10adba2ff547';
    $display->content['new-77f7e9f8-0bb3-41ad-b611-10adba2ff547'] = $pane;
    $display->panels['center_right'][1] = 'new-77f7e9f8-0bb3-41ad-b611-10adba2ff547';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['site_administration'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'user_administration';
  $page->task = 'page';
  $page->admin_title = 'User administration';
  $page->admin_description = '';
  $page->path = 'admin/site_admin/user';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 5,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'action',
    'title' => '用户管理',
    'name' => 'navigation',
    'weight' => '1',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_user_administration_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'user_administration';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '用户管理';
  $display->uuid = 'f748ea2f-1fa7-4d67-b854-dd873fc04611';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b3d6a157-89ee-4ac8-b951-0aa96bdb1c36';
    $pane->panel = 'center';
    $pane->type = 'views';
    $pane->subtype = 'user';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '30',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b3d6a157-89ee-4ac8-b951-0aa96bdb1c36';
    $display->content['new-b3d6a157-89ee-4ac8-b951-0aa96bdb1c36'] = $pane;
    $display->panels['center'][0] = 'new-b3d6a157-89ee-4ac8-b951-0aa96bdb1c36';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['user_administration'] = $page;

  return $pages;

}
