<?php
/**
 * @file
 * whg_page.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function whg_page_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'title' => '视频一',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'video',
  'language' => 'und',
  'created' => 1415264278,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '080eddfb-9eb6-4e78-ac52-e78c03075638',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '视频一的描述',
        'summary' => '',
        'format' => NULL,
        'safe_summary' => '',
      ),
    ),
  ),
  'field_resp_by' => array(),
  'field_tr_liability_manner_video' => array(),
  'field_tr_resource_category' => array(
    'und' => array(
      0 => array(
        'tid' => 'eae4e767-2097-458b-bf71-7adb49bc3549',
        'uuid' => 'eae4e767-2097-458b-bf71-7adb49bc3549',
      ),
    ),
  ),
  'field_area' => array(),
  'field_date' => array(),
  'field_tr_language' => array(
    'und' => array(
      0 => array(
        'tid' => '2b443640-db48-4efa-abb2-780fa745c3f8',
        'uuid' => '2b443640-db48-4efa-abb2-780fa745c3f8',
      ),
    ),
  ),
  'field_provenance' => array(),
  'field_tags' => array(),
  'field_copyright_owner' => array(),
  'field_copyright' => array(),
  'field_file' => array(
    'und' => array(
      0 => array(
        'fid' => 9,
        'filename' => 'videoviewdemo.mp4',
        'uri' => 'public://videoviewdemo.mp4',
        'filemime' => 'video/mp4',
        'filesize' => 1393457,
        'status' => 1,
        'timestamp' => 1415264278,
        'type' => 'video',
        'uuid' => '10ed9be6-40a7-435a-bc2f-44de127c19b3',
        'rdf_mapping' => array(),
        'metadata' => array(),
        'alt' => '',
        'title' => '',
        'display' => 1,
        'description' => '',
        'user_uuid' => 'e3fd792d-567e-4d5b-8f63-5350e80ba31c',
      ),
    ),
  ),
  'field_er_people' => array(
    'und' => array(
      0 => array(
        'target_id' => '80d73cd9-a455-4345-bff6-b768d919962d',
        'uuid' => '80d73cd9-a455-4345-bff6-b768d919962d',
      ),
    ),
  ),
  'field_tr_menu_item' => array(
    'und' => array(
      0 => array(
        'tid' => '546f884f-0a0f-4c0e-bacc-e087f4877cd7',
        'uuid' => '546f884f-0a0f-4c0e-bacc-e087f4877cd7',
      ),
      1 => array(
        'tid' => '649d5f55-2c13-42c2-bde9-bcdf66bf6748',
        'uuid' => '649d5f55-2c13-42c2-bde9-bcdf66bf6748',
      ),
      2 => array(
        'tid' => '164560dc-054c-4de8-8e5b-daf1182aed68',
        'uuid' => '164560dc-054c-4de8-8e5b-daf1182aed68',
      ),
      3 => array(
        'tid' => '5409e8f2-fb9c-4f0c-a740-a5518a10dc0e',
        'uuid' => '5409e8f2-fb9c-4f0c-a740-a5518a10dc0e',
      ),
      4 => array(
        'tid' => '4c392a83-a1db-4017-aea9-327b8774d088',
        'uuid' => '4c392a83-a1db-4017-aea9-327b8774d088',
      ),
      5 => array(
        'tid' => '1037a5f0-a39f-49ea-aa40-ae83bf269de8',
        'uuid' => '1037a5f0-a39f-49ea-aa40-ae83bf269de8',
      ),
      6 => array(
        'tid' => 'c91a33b5-526f-4a7f-aaae-892fd99fcf0e',
        'uuid' => 'c91a33b5-526f-4a7f-aaae-892fd99fcf0e',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2014-11-06 16:57:58 +0800',
  'user_uuid' => 'e3fd792d-567e-4d5b-8f63-5350e80ba31c',
);
  $nodes[] = array(
  'title' => '正字戏综合概述',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'article',
  'language' => 'und',
  'created' => 1415180588,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '0aa0b35a-f0d9-4898-8d1f-e7edd6bb974c',
  'field_resp_by' => array(),
  'field_tr_liability_manner_articl' => array(),
  'field_tr_resource_category' => array(
    'und' => array(
      0 => array(
        'tid' => 'eae4e767-2097-458b-bf71-7adb49bc3549',
        'uuid' => 'eae4e767-2097-458b-bf71-7adb49bc3549',
      ),
    ),
  ),
  'field_tr_language' => array(
    'und' => array(
      0 => array(
        'tid' => '2b443640-db48-4efa-abb2-780fa745c3f8',
        'uuid' => '2b443640-db48-4efa-abb2-780fa745c3f8',
      ),
    ),
  ),
  'field_provenance' => array(),
  'field_tags' => array(),
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '描述',
        'summary' => '',
        'format' => NULL,
        'safe_summary' => '',
      ),
    ),
  ),
  'field_copyright_owner' => array(),
  'field_copyright' => array(),
  'field_content' => array(
    'und' => array(
      0 => array(
        'value' => '正字戏：又名“正音戏”，因其语言用中州官话（闽南、潮州等地称为“正音”或“正字”）而得名，流行于广东省海丰、陆丰、潮汕和福建省闽南、台湾省等地。形成于明宣德年间，是元明南戏的一支。

约有五百多年的历史，主要曲调有正音曲、昆曲两种，也有部分杂曲，小调。伴奏乐器“正音曲”以龙舌兰壳制的大管弦为主，配以三弦，竹弦等；昆曲、杂曲等以笛和唢呐为主。传统剧目分文戏和武戏两类，解放后，整理改编的剧目以《槐荫别》、《百花赠剑》、《古城会》、《张飞归家》影响较大。',
        'summary' => '',
        'format' => NULL,
        'safe_summary' => '',
      ),
    ),
  ),
  'field_er_people' => array(),
  'field_tr_menu_item' => array(
    'und' => array(
      0 => array(
        'tid' => '546f884f-0a0f-4c0e-bacc-e087f4877cd7',
        'uuid' => '546f884f-0a0f-4c0e-bacc-e087f4877cd7',
      ),
      1 => array(
        'tid' => '649d5f55-2c13-42c2-bde9-bcdf66bf6748',
        'uuid' => '649d5f55-2c13-42c2-bde9-bcdf66bf6748',
      ),
      2 => array(
        'tid' => '164560dc-054c-4de8-8e5b-daf1182aed68',
        'uuid' => '164560dc-054c-4de8-8e5b-daf1182aed68',
      ),
      3 => array(
        'tid' => '5409e8f2-fb9c-4f0c-a740-a5518a10dc0e',
        'uuid' => '5409e8f2-fb9c-4f0c-a740-a5518a10dc0e',
      ),
      4 => array(
        'tid' => '4c392a83-a1db-4017-aea9-327b8774d088',
        'uuid' => '4c392a83-a1db-4017-aea9-327b8774d088',
      ),
      5 => array(
        'tid' => '1037a5f0-a39f-49ea-aa40-ae83bf269de8',
        'uuid' => '1037a5f0-a39f-49ea-aa40-ae83bf269de8',
      ),
      6 => array(
        'tid' => 'c91a33b5-526f-4a7f-aaae-892fd99fcf0e',
        'uuid' => 'c91a33b5-526f-4a7f-aaae-892fd99fcf0e',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'field_image' => array(
      'predicates' => array(
        0 => 'og:image',
        1 => 'rdfs:seeAlso',
      ),
      'type' => 'rel',
    ),
    'field_tags' => array(
      'predicates' => array(
        0 => 'dc:subject',
      ),
      'type' => 'rel',
    ),
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2014-11-05 17:43:08 +0800',
  'user_uuid' => 'e3fd792d-567e-4d5b-8f63-5350e80ba31c',
);
  $nodes[] = array(
  'title' => '正字戏综合概述图片三',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'picture',
  'language' => 'und',
  'created' => 1415262441,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '0c82fea2-2bd9-4d0f-aa06-4e1b4befe89f',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '图片三的描述',
        'summary' => '',
        'format' => NULL,
        'safe_summary' => '',
      ),
    ),
  ),
  'field_resp_by' => array(),
  'field_tr_resource_category' => array(
    'und' => array(
      0 => array(
        'tid' => 'eae4e767-2097-458b-bf71-7adb49bc3549',
        'uuid' => 'eae4e767-2097-458b-bf71-7adb49bc3549',
      ),
    ),
  ),
  'field_area' => array(),
  'field_date' => array(),
  'field_provenance' => array(),
  'field_tags' => array(),
  'field_copyright_owner' => array(),
  'field_copyright' => array(),
  'field_tr_liability_manner_pic' => array(),
  'field_er_people' => array(),
  'field_tr_menu_item' => array(
    'und' => array(
      0 => array(
        'tid' => '546f884f-0a0f-4c0e-bacc-e087f4877cd7',
        'uuid' => '546f884f-0a0f-4c0e-bacc-e087f4877cd7',
      ),
      1 => array(
        'tid' => '649d5f55-2c13-42c2-bde9-bcdf66bf6748',
        'uuid' => '649d5f55-2c13-42c2-bde9-bcdf66bf6748',
      ),
    ),
  ),
  'field_image' => array(
    'und' => array(
      0 => array(
        'fid' => 6,
        'filename' => '1稀有剧种基本信息页面—环境.jpg',
        'uri' => 'public://1稀有剧种基本信息页面—环境.jpg',
        'filemime' => 'image/jpeg',
        'filesize' => 71011,
        'status' => 1,
        'timestamp' => 1415262441,
        'type' => 'image',
        'uuid' => '9fee26d4-a3c4-4451-9440-960b590cbc57',
        'field_file_image_alt_text' => array(),
        'field_file_image_title_text' => array(),
        'rdf_mapping' => array(),
        'metadata' => array(
          'height' => 832,
          'width' => 758,
        ),
        'alt' => '',
        'title' => '',
        'width' => 758,
        'height' => 832,
        'user_uuid' => 'e3fd792d-567e-4d5b-8f63-5350e80ba31c',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2014-11-06 16:27:21 +0800',
  'user_uuid' => 'e3fd792d-567e-4d5b-8f63-5350e80ba31c',
);
  $nodes[] = array(
  'title' => '正字戏综合概述图片一',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'picture',
  'language' => 'und',
  'created' => 1415256903,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '2bd8a351-1cc4-4e52-b1c8-b5d12f8872a2',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '这里是图片的描述',
        'summary' => '',
        'format' => NULL,
        'safe_summary' => '',
      ),
    ),
  ),
  'field_resp_by' => array(),
  'field_tr_resource_category' => array(
    'und' => array(
      0 => array(
        'tid' => 'eae4e767-2097-458b-bf71-7adb49bc3549',
        'uuid' => 'eae4e767-2097-458b-bf71-7adb49bc3549',
      ),
    ),
  ),
  'field_area' => array(),
  'field_date' => array(),
  'field_provenance' => array(),
  'field_tags' => array(),
  'field_copyright_owner' => array(),
  'field_copyright' => array(),
  'field_tr_liability_manner_pic' => array(),
  'field_er_people' => array(
    'und' => array(
      0 => array(
        'target_id' => '80d73cd9-a455-4345-bff6-b768d919962d',
        'uuid' => '80d73cd9-a455-4345-bff6-b768d919962d',
      ),
    ),
  ),
  'field_tr_menu_item' => array(
    'und' => array(
      0 => array(
        'tid' => '546f884f-0a0f-4c0e-bacc-e087f4877cd7',
        'uuid' => '546f884f-0a0f-4c0e-bacc-e087f4877cd7',
      ),
      1 => array(
        'tid' => '649d5f55-2c13-42c2-bde9-bcdf66bf6748',
        'uuid' => '649d5f55-2c13-42c2-bde9-bcdf66bf6748',
      ),
      2 => array(
        'tid' => '164560dc-054c-4de8-8e5b-daf1182aed68',
        'uuid' => '164560dc-054c-4de8-8e5b-daf1182aed68',
      ),
      3 => array(
        'tid' => '5409e8f2-fb9c-4f0c-a740-a5518a10dc0e',
        'uuid' => '5409e8f2-fb9c-4f0c-a740-a5518a10dc0e',
      ),
      4 => array(
        'tid' => '4c392a83-a1db-4017-aea9-327b8774d088',
        'uuid' => '4c392a83-a1db-4017-aea9-327b8774d088',
      ),
    ),
  ),
  'field_image' => array(
    'und' => array(
      0 => array(
        'fid' => 4,
        'filename' => '1稀有剧种基本信息页面—综合概述.jpg',
        'uri' => 'public://1稀有剧种基本信息页面—综合概述.jpg',
        'filemime' => 'image/jpeg',
        'filesize' => 62350,
        'status' => 1,
        'timestamp' => 1415256891,
        'type' => 'image',
        'uuid' => 'a57bed53-724f-4f4d-b4fc-5536c560983f',
        'field_file_image_alt_text' => array(),
        'field_file_image_title_text' => array(),
        'rdf_mapping' => array(),
        'metadata' => array(
          'height' => 684,
          'width' => 758,
        ),
        'alt' => '',
        'title' => '',
        'width' => 758,
        'height' => 684,
        'user_uuid' => 'e3fd792d-567e-4d5b-8f63-5350e80ba31c',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2014-11-06 14:55:03 +0800',
  'user_uuid' => 'e3fd792d-567e-4d5b-8f63-5350e80ba31c',
);
  $nodes[] = array(
  'title' => '正字戏的形成',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'article',
  'language' => 'und',
  'created' => 1415181893,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '2be71195-167a-48b7-8f66-c53158e65472',
  'field_resp_by' => array(),
  'field_tr_liability_manner_articl' => array(),
  'field_tr_resource_category' => array(
    'und' => array(
      0 => array(
        'tid' => 'eae4e767-2097-458b-bf71-7adb49bc3549',
        'uuid' => 'eae4e767-2097-458b-bf71-7adb49bc3549',
      ),
    ),
  ),
  'field_tr_language' => array(
    'und' => array(
      0 => array(
        'tid' => '2b443640-db48-4efa-abb2-780fa745c3f8',
        'uuid' => '2b443640-db48-4efa-abb2-780fa745c3f8',
      ),
    ),
  ),
  'field_provenance' => array(),
  'field_tags' => array(),
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '描述',
        'summary' => '',
        'format' => NULL,
        'safe_summary' => '',
      ),
    ),
  ),
  'field_copyright_owner' => array(),
  'field_copyright' => array(),
  'field_content' => array(
    'und' => array(
      0 => array(
        'value' => '正字戏的来源与南戏有关。明万历年间黄绅撰《石桥场土城记》谓陆丰县碣石卫"国初以来，歌舞相闻"。嘉靖以后戏台建筑成群，万历年间在碣石玄武山元山寺前修建戏台专演正字戏，并于清乾隆十三年(1748年)把戏台扩建成可容两个戏班合演的大戏台。元山寺修有供艺人食宿的戏馆，西库房墙壁写着乾隆十五年(1750年)至1961年的玄武山历届神诞戏"总理"的名字。

据说最古老的正字戏班社双喜班，乾隆四十二年创建后持续活动至1960年。清朝中叶，正字戏在潮州一带演出甚为活跃，咸丰十年(1860年)重修潮州田元帅庙时，碑文记"正音班每年每班银二元"敬神，捐银数多于潮音班1倍。这时的常演剧目有"四大苦戏"《琵琶记》、《白兔记》、《荆钗记》、《葵花记》，"四大弓马戏"《铁弓缘》、《马陵道》、《千里驹》、《义忠烈》，"四大喜戏"，《三元记》、《五桂记》、《满床笏》、《月华记》。这些剧目以唱正音曲为主，昆曲次之。现存同治十一年(1872年)贝淑仪手抄《荆钗记》演出本，标有生、旦、外、占、末、丑、净、夫、小、杂10种演出行当。

光绪、宣统年间，"在潮州具有古代艺术文化价值的正音戏……尚有万利班、老永丰班、新永丰班、老三胜班、新三胜班……来往潮州各县演唱……忆儿时爱着老三胜、新永丰班之跳火圈、跳剑窗、吊辫。老三胜班演白鸟记，其骑布马之台步，亦博得彩声。午夜，诸班多串演潮音，以冀迎合大众"。(萧遥天《潮州戏剧志》)光绪三十二年(1906年)陆丰碣石玄武山祖庙重光，除聘请常于该处演出的正字戏班演出外，还云集6个剧种的10多个戏班助庆。这时也有正字戏班远涉重洋到东南亚演出。清末民初，正字戏渐次退出潮州，偏处海丰、陆丰两县演出提纲戏维持。正生郑乃二遗留"宣统元年岁次已酉二月十五日置"的一本提纲戏集载有十多个剧目，除一出《鸿门宴》外，其余均为"三国戏"。是时的正字戏"言语拉杂，戏皆道白无唱，所演如《三国志》、《前后唐》、《征东》、《征西》等……出台喜打大鼓，吹大喇叭"。(胡朴安《中华全国风俗志》)为了应付提纲戏的演出，戏班的戏箱置备许多演义小说供编演时参阅。艺人掌握着由特定的吹打牌子和表演排场，便可对付演出。',
        'summary' => '',
        'format' => NULL,
        'safe_summary' => '',
      ),
    ),
  ),
  'field_er_people' => array(),
  'field_tr_menu_item' => array(
    'und' => array(
      0 => array(
        'tid' => '62e9f1b5-2a61-47cf-b3ac-946858fb303f',
        'uuid' => '62e9f1b5-2a61-47cf-b3ac-946858fb303f',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'field_image' => array(
      'predicates' => array(
        0 => 'og:image',
        1 => 'rdfs:seeAlso',
      ),
      'type' => 'rel',
    ),
    'field_tags' => array(
      'predicates' => array(
        0 => 'dc:subject',
      ),
      'type' => 'rel',
    ),
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2014-11-05 18:04:53 +0800',
  'user_uuid' => 'e3fd792d-567e-4d5b-8f63-5350e80ba31c',
);
  $nodes[] = array(
  'title' => '正字戏综合概述图片二',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'picture',
  'language' => 'und',
  'created' => 1415262321,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '722e27ba-35cb-49dd-b0ce-7d5c513ff672',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '图片二的描述',
        'summary' => '',
        'format' => NULL,
        'safe_summary' => '',
      ),
    ),
  ),
  'field_resp_by' => array(),
  'field_tr_resource_category' => array(
    'und' => array(
      0 => array(
        'tid' => 'eae4e767-2097-458b-bf71-7adb49bc3549',
        'uuid' => 'eae4e767-2097-458b-bf71-7adb49bc3549',
      ),
    ),
  ),
  'field_area' => array(),
  'field_date' => array(),
  'field_provenance' => array(),
  'field_tags' => array(),
  'field_copyright_owner' => array(),
  'field_copyright' => array(),
  'field_tr_liability_manner_pic' => array(),
  'field_er_people' => array(),
  'field_tr_menu_item' => array(
    'und' => array(
      0 => array(
        'tid' => '546f884f-0a0f-4c0e-bacc-e087f4877cd7',
        'uuid' => '546f884f-0a0f-4c0e-bacc-e087f4877cd7',
      ),
      1 => array(
        'tid' => '649d5f55-2c13-42c2-bde9-bcdf66bf6748',
        'uuid' => '649d5f55-2c13-42c2-bde9-bcdf66bf6748',
      ),
    ),
  ),
  'field_image' => array(
    'und' => array(
      0 => array(
        'fid' => 5,
        'filename' => ' .jpg',
        'uri' => 'public:// .jpg',
        'filemime' => 'image/jpeg',
        'filesize' => 41399,
        'status' => 1,
        'timestamp' => 1415262321,
        'type' => 'image',
        'uuid' => '59294227-dccf-401a-aff0-b03cd7e57344',
        'field_file_image_alt_text' => array(),
        'field_file_image_title_text' => array(),
        'rdf_mapping' => array(),
        'metadata' => array(
          'height' => 832,
          'width' => 758,
        ),
        'alt' => '',
        'title' => '',
        'width' => 758,
        'height' => 832,
        'user_uuid' => 'e3fd792d-567e-4d5b-8f63-5350e80ba31c',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2014-11-06 16:25:21 +0800',
  'user_uuid' => 'e3fd792d-567e-4d5b-8f63-5350e80ba31c',
);
  $nodes[] = array(
  'title' => '音频一',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'audio',
  'language' => 'und',
  'created' => 1415263765,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '7944e300-fad4-4f19-a372-30b30736e9f4',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '音频一的描述',
        'summary' => '',
        'format' => NULL,
        'safe_summary' => '',
      ),
    ),
  ),
  'field_resp_by' => array(),
  'field_tr_resource_category' => array(
    'und' => array(
      0 => array(
        'tid' => 'eae4e767-2097-458b-bf71-7adb49bc3549',
        'uuid' => 'eae4e767-2097-458b-bf71-7adb49bc3549',
      ),
    ),
  ),
  'field_area' => array(),
  'field_date' => array(),
  'field_tr_language' => array(),
  'field_provenance' => array(),
  'field_tags' => array(),
  'field_copyright_owner' => array(),
  'field_copyright' => array(),
  'field_file' => array(
    'und' => array(
      0 => array(
        'fid' => 8,
        'filename' => '001.mp3',
        'uri' => 'public://001_0.mp3',
        'filemime' => 'audio/mpeg',
        'filesize' => 5541136,
        'status' => 1,
        'timestamp' => 1415263765,
        'type' => 'audio',
        'uuid' => 'a8c065d2-a70e-4eb7-9252-a09521556514',
        'rdf_mapping' => array(),
        'metadata' => array(),
        'alt' => '',
        'title' => '',
        'display' => 1,
        'description' => '',
        'user_uuid' => 'e3fd792d-567e-4d5b-8f63-5350e80ba31c',
      ),
    ),
  ),
  'field_tr_liability_manner_audio' => array(),
  'field_er_people' => array(
    'und' => array(
      0 => array(
        'target_id' => '80d73cd9-a455-4345-bff6-b768d919962d',
        'uuid' => '80d73cd9-a455-4345-bff6-b768d919962d',
      ),
    ),
  ),
  'field_tr_menu_item' => array(
    'und' => array(
      0 => array(
        'tid' => '546f884f-0a0f-4c0e-bacc-e087f4877cd7',
        'uuid' => '546f884f-0a0f-4c0e-bacc-e087f4877cd7',
      ),
      1 => array(
        'tid' => '649d5f55-2c13-42c2-bde9-bcdf66bf6748',
        'uuid' => '649d5f55-2c13-42c2-bde9-bcdf66bf6748',
      ),
      2 => array(
        'tid' => '164560dc-054c-4de8-8e5b-daf1182aed68',
        'uuid' => '164560dc-054c-4de8-8e5b-daf1182aed68',
      ),
      3 => array(
        'tid' => '5409e8f2-fb9c-4f0c-a740-a5518a10dc0e',
        'uuid' => '5409e8f2-fb9c-4f0c-a740-a5518a10dc0e',
      ),
      4 => array(
        'tid' => '4c392a83-a1db-4017-aea9-327b8774d088',
        'uuid' => '4c392a83-a1db-4017-aea9-327b8774d088',
      ),
      5 => array(
        'tid' => '1037a5f0-a39f-49ea-aa40-ae83bf269de8',
        'uuid' => '1037a5f0-a39f-49ea-aa40-ae83bf269de8',
      ),
      6 => array(
        'tid' => 'c91a33b5-526f-4a7f-aaae-892fd99fcf0e',
        'uuid' => 'c91a33b5-526f-4a7f-aaae-892fd99fcf0e',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2014-11-06 16:49:25 +0800',
  'user_uuid' => 'e3fd792d-567e-4d5b-8f63-5350e80ba31c',
);
  $nodes[] = array(
  'title' => '人物一',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'person',
  'language' => 'und',
  'created' => 1415262985,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '80d73cd9-a455-4345-bff6-b768d919962d',
  'field_alias' => array(
    'und' => array(
      0 => array(
        'value' => '别名',
        'format' => NULL,
      ),
    ),
  ),
  'field_stage_name' => array(
    'und' => array(
      0 => array(
        'value' => '艺名',
        'format' => NULL,
      ),
    ),
  ),
  'field_tags' => array(),
  'field_tr_duty_type' => array(
    'und' => array(
      0 => array(
        'tid' => 'c2fa6e81-1544-4967-b439-7114277b1749',
        'uuid' => 'c2fa6e81-1544-4967-b439-7114277b1749',
      ),
      1 => array(
        'tid' => '04635b0e-b5c6-4172-afec-dbfd71a91dda',
        'uuid' => '04635b0e-b5c6-4172-afec-dbfd71a91dda',
      ),
    ),
  ),
  'field_tr_resource_category' => array(
    'und' => array(
      0 => array(
        'tid' => 'eae4e767-2097-458b-bf71-7adb49bc3549',
        'uuid' => 'eae4e767-2097-458b-bf71-7adb49bc3549',
      ),
    ),
  ),
  'field_gender' => array(
    'und' => array(
      0 => array(
        'value' => '男',
      ),
    ),
  ),
  'field_ancestry' => array(
    'und' => array(
      0 => array(
        'value' => '籍贯',
        'format' => NULL,
      ),
    ),
  ),
  'field_place_of_birth' => array(
    'und' => array(
      0 => array(
        'value' => '出生地',
        'format' => NULL,
      ),
    ),
  ),
  'field_date_of_birth' => array(),
  'field_date_of_death' => array(),
  'field_nationality' => array(
    'und' => array(
      0 => array(
        'value' => '汉族',
        'format' => NULL,
      ),
    ),
  ),
  'field_home_address' => array(
    'und' => array(
      0 => array(
        'value' => '家庭住址',
        'format' => NULL,
      ),
    ),
  ),
  'field_institution' => array(
    'und' => array(
      0 => array(
        'value' => '供职单位',
        'format' => NULL,
      ),
    ),
  ),
  'field_basic_info' => array(
    'und' => array(
      0 => array(
        'value' => '艺人基本信息',
        'format' => NULL,
      ),
    ),
  ),
  'field_experience' => array(
    'und' => array(
      0 => array(
        'value' => '学艺经历',
        'format' => NULL,
      ),
    ),
  ),
  'field_troupe' => array(
    'und' => array(
      0 => array(
        'value' => '班设和剧团',
        'format' => NULL,
      ),
    ),
  ),
  'field_typical_work' => array(
    'und' => array(
      0 => array(
        'value' => '代表剧目',
        'format' => NULL,
      ),
    ),
  ),
  'field_artistic_characteristic' => array(
    'und' => array(
      0 => array(
        'value' => '艺术特点',
        'format' => NULL,
      ),
    ),
  ),
  'field_achievement' => array(
    'und' => array(
      0 => array(
        'value' => '成就和影响',
        'format' => NULL,
      ),
    ),
  ),
  'field_heritage' => array(
    'und' => array(
      0 => array(
        'value' => '传承情况',
        'format' => NULL,
      ),
    ),
  ),
  'field_photo' => array(
    'und' => array(
      0 => array(
        'fid' => 7,
        'filename' => '3b292df5e0fe992505f1817d37a85edf8db1711a.jpg.gif',
        'uri' => 'public://3b292df5e0fe992505f1817d37a85edf8db1711a.jpg.gif',
        'filemime' => 'image/gif',
        'filesize' => 137745,
        'status' => 1,
        'timestamp' => 1415262985,
        'type' => 'image',
        'uuid' => '95df4f9e-ddbd-439e-8233-c596809b417f',
        'field_file_image_alt_text' => array(),
        'field_file_image_title_text' => array(),
        'rdf_mapping' => array(),
        'metadata' => array(
          'height' => 334,
          'width' => 500,
        ),
        'alt' => '',
        'title' => '',
        'width' => 500,
        'height' => 334,
        'user_uuid' => 'e3fd792d-567e-4d5b-8f63-5350e80ba31c',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2014-11-06 16:36:25 +0800',
  'user_uuid' => 'e3fd792d-567e-4d5b-8f63-5350e80ba31c',
);
  return $nodes;
}
