<?php
/**
 * @file
 * whg_banner_slideshow.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function whg_banner_slideshow_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'bean-slideshow-field_images'
  $field_instances['bean-slideshow-field_images'] = array(
    'bundle' => 'slideshow',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'nivo_formatter',
        'settings' => array(
          'nivo_formatter_image_style' => 'slider_full',
          'nivo_formatter_nivo_slider' => array(
            'animSpeed' => 500,
            'boxCols' => 8,
            'boxRows' => 4,
            'captionOpacity' => 0.8,
            'controlNav' => 0,
            'directionNav' => 1,
            'directionNavHide' => TRUE,
            'effect' => array(
              'boxRandom' => 'boxRandom',
              'fade' => 'fade',
              'fold' => 'fold',
              'sliceUpDown' => 'sliceUpDown',
            ),
            'keyboardNav' => TRUE,
            'manualAdvance' => 0,
            'nextText' => 'Next',
            'pauseOnHover' => TRUE,
            'pauseTime' => 5000,
            'prevText' => 'Prev',
            'slices' => 15,
            'startSlide' => 0,
          ),
          'nivo_formatter_theme' => 'default',
          'nivo_slider_thumbnail' => array(
            'controlNavThumbs' => 0,
            'thumbnail_style' => 'thumbnail',
          ),
        ),
        'type' => 'nivo_formatter',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bean',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_images',
    'label' => '图片',
    'required' => 1,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => 'slideshow',
      'file_extensions' => 'png gif jpg jpeg',
      'focus' => 0,
      'focus_lock_ratio' => 0,
      'focus_min_size' => '',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'medium',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 12,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('图片');

  return $field_instances;
}
