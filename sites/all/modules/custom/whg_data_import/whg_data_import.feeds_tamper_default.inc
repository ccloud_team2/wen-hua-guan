<?php
/**
 * @file
 * whg_data_import.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function whg_data_import_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'data_import_audio-__-explode';
  $feeds_tamper->importer = 'data_import_audio';
  $feeds_tamper->source = '主题';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '%s',
    'limit' => '',
    'real_separator' => ' ',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode';
  $export['data_import_audio-__-explode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'data_import_audio-__-explode_lang';
  $feeds_tamper->importer = 'data_import_audio';
  $feeds_tamper->source = '语种';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '%s',
    'limit' => '',
    'real_separator' => ' ',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode';
  $export['data_import_audio-__-explode_lang'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'data_import_audio-____-efq_finder';
  $feeds_tamper->importer = 'data_import_audio';
  $feeds_tamper->source = '相关人物';
  $feeds_tamper->plugin_id = 'efq_finder';
  $feeds_tamper->settings = array(
    'update' => '更新',
    'entity_type' => 'node',
    'bundle' => 'person',
    'field' => 'title',
    'property' => TRUE,
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Entity Field Query finder';
  $export['data_import_audio-____-efq_finder'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'data_import_audio-____-explode_person';
  $feeds_tamper->importer = 'data_import_audio';
  $feeds_tamper->source = '相关人物';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '%s',
    'limit' => '',
    'real_separator' => ' ',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Explode';
  $export['data_import_audio-____-explode_person'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'data_import_audio-_____-explode_menu_item';
  $feeds_tamper->importer = 'data_import_audio';
  $feeds_tamper->source = '所属菜单项';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '--',
    'limit' => '',
    'real_separator' => '--',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Explode';
  $export['data_import_audio-_____-explode_menu_item'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'data_import_person-__-explode';
  $feeds_tamper->importer = 'data_import_person';
  $feeds_tamper->source = '主题';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '%s',
    'limit' => '',
    'real_separator' => ' ',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode';
  $export['data_import_person-__-explode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'data_import_picture-__-explode';
  $feeds_tamper->importer = 'data_import_picture';
  $feeds_tamper->source = '主题';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '%s',
    'limit' => '',
    'real_separator' => ' ',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode';
  $export['data_import_picture-__-explode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'data_import_picture-____-efq_finder';
  $feeds_tamper->importer = 'data_import_picture';
  $feeds_tamper->source = '相关人物';
  $feeds_tamper->plugin_id = 'efq_finder';
  $feeds_tamper->settings = array(
    'update' => '更新',
    'entity_type' => 'node',
    'bundle' => 'person',
    'field' => 'title',
    'property' => TRUE,
  );
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'Entity Field Query finder';
  $export['data_import_picture-____-efq_finder'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'data_import_picture-____-explode_person';
  $feeds_tamper->importer = 'data_import_picture';
  $feeds_tamper->source = '相关人物';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '%s',
    'limit' => '',
    'real_separator' => ' ',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Explode';
  $export['data_import_picture-____-explode_person'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'data_import_picture-_____-explode_menu_item';
  $feeds_tamper->importer = 'data_import_picture';
  $feeds_tamper->source = '所属菜单项';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '--',
    'limit' => '',
    'real_separator' => '--',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Explode';
  $export['data_import_picture-_____-explode_menu_item'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'data_import_video-__-explode';
  $feeds_tamper->importer = 'data_import_video';
  $feeds_tamper->source = '主题';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '%s',
    'limit' => '',
    'real_separator' => ' ',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode';
  $export['data_import_video-__-explode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'data_import_video-__-explode_lang';
  $feeds_tamper->importer = 'data_import_video';
  $feeds_tamper->source = '语种';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '%s',
    'limit' => '',
    'real_separator' => ' ',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode';
  $export['data_import_video-__-explode_lang'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'data_import_video-____-efq_finder';
  $feeds_tamper->importer = 'data_import_video';
  $feeds_tamper->source = '相关人物';
  $feeds_tamper->plugin_id = 'efq_finder';
  $feeds_tamper->settings = array(
    'update' => '更新',
    'entity_type' => 'node',
    'bundle' => 'person',
    'field' => 'title',
    'property' => TRUE,
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Entity Field Query finder';
  $export['data_import_video-____-efq_finder'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'data_import_video-____-explode_person';
  $feeds_tamper->importer = 'data_import_video';
  $feeds_tamper->source = '相关人物';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '%s',
    'limit' => '',
    'real_separator' => ' ',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Explode';
  $export['data_import_video-____-explode_person'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'data_import_video-_____-explode_menu_item';
  $feeds_tamper->importer = 'data_import_video';
  $feeds_tamper->source = '所属菜单项';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '--',
    'limit' => '',
    'real_separator' => '--',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Explode';
  $export['data_import_video-_____-explode_menu_item'] = $feeds_tamper;

  return $export;
}
