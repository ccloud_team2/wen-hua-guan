<?php
/**
 * @file
 * whg_data_import.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function whg_data_import_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'imported_items';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Imported items';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = '更多';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = '应用';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = '重置';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = '排序依据';
  $handler->display->display_options['exposed_form']['options']['sort_asc_label'] = '升序';
  $handler->display->display_options['exposed_form']['options']['sort_desc_label'] = '降序';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '100';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = '每页条目数';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- 全部 -';
  $handler->display->display_options['pager']['options']['expose']['offset_label'] = '偏移量';
  $handler->display->display_options['pager']['options']['tags']['first'] = '« 第一页';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ 前一页';
  $handler->display->display_options['pager']['options']['tags']['next'] = '下一页 ›';
  $handler->display->display_options['pager']['options']['tags']['last'] = '末页 »';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'type' => 'type',
    'edit_node' => 'edit_node',
    'field_photo' => 'field_photo',
    'title' => 'title',
    'field_tr_menu_item' => 'field_tr_menu_item',
    'field_alias' => 'field_alias',
    'field_stage_name' => 'field_stage_name',
    'field_er_people' => 'field_er_people',
    'field_tr_resource_category' => 'field_tr_resource_category',
    'field_area' => 'field_area',
    'field_date' => 'field_date',
    'field_tr_language' => 'field_tr_language',
    'field_provenance' => 'field_provenance',
    'field_tags' => 'field_tags',
    'body' => 'body',
    'field_tr_duty_type' => 'field_tr_duty_type',
    'field_date_of_birth' => 'field_date_of_birth',
    'field_date_of_death' => 'field_date_of_death',
    'field_gender' => 'field_gender',
    'field_ancestry' => 'field_ancestry',
    'field_place_of_birth' => 'field_place_of_birth',
    'field_nationality' => 'field_nationality',
    'field_home_address' => 'field_home_address',
    'field_institution' => 'field_institution',
    'field_basic_info' => 'field_basic_info',
    'field_experience' => 'field_experience',
    'field_troupe' => 'field_troupe',
    'field_typical_work' => 'field_typical_work',
    'field_artistic_characteristic' => 'field_artistic_characteristic',
    'field_achievement' => 'field_achievement',
    'field_heritage' => 'field_heritage',
    'field_image' => 'field_image',
    'field_file' => 'field_file',
    'field_copyright_owner' => 'field_copyright_owner',
    'field_copyright' => 'field_copyright',
    'field_tr_liability_manner_audio' => 'field_tr_liability_manner_audio',
    'field_tr_liability_manner_video' => 'field_tr_liability_manner_video',
    'field_tr_liability_manner_pic' => 'field_tr_liability_manner_pic',
    'field_tr_liability_manner_articl' => 'field_tr_liability_manner_articl',
    'field_resp_by' => 'field_resp_by',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'type' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'edit_node' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_photo' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_tr_menu_item' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_alias' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_stage_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_er_people' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_tr_resource_category' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_area' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_date' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_tr_language' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_provenance' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_tags' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'body' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_tr_duty_type' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_date_of_birth' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_date_of_death' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_gender' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_ancestry' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_place_of_birth' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_nationality' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_home_address' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_institution' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_basic_info' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_experience' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_troupe' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_typical_work' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_artistic_characteristic' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_achievement' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_heritage' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_image' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_file' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_copyright_owner' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_copyright' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_tr_liability_manner_audio' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_tr_liability_manner_video' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_tr_liability_manner_pic' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_tr_liability_manner_articl' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
    'field_resp_by' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
  );
  /* 关联: Feeds item: Owner feed */
  $handler->display->display_options['relationships']['feed_nid']['id'] = 'feed_nid';
  $handler->display->display_options['relationships']['feed_nid']['table'] = 'feeds_item';
  $handler->display->display_options['relationships']['feed_nid']['field'] = 'feed_nid';
  $handler->display->display_options['relationships']['feed_nid']['label'] = 'Feed owner';
  $handler->display->display_options['relationships']['feed_nid']['required'] = TRUE;
  /* 字段: 内容: 类型 */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'node';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  /* 字段: 内容: 编辑链接 */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = '编辑';
  $handler->display->display_options['fields']['edit_node']['text'] = '编辑';
  /* 字段: 内容: 艺人照片 */
  $handler->display->display_options['fields']['field_photo']['id'] = 'field_photo';
  $handler->display->display_options['fields']['field_photo']['table'] = 'field_data_field_photo';
  $handler->display->display_options['fields']['field_photo']['field'] = 'field_photo';
  $handler->display->display_options['fields']['field_photo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_photo']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  /* 字段: 内容: 标题 */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '名称';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* 字段: 字段: 菜单项 */
  $handler->display->display_options['fields']['field_tr_menu_item']['id'] = 'field_tr_menu_item';
  $handler->display->display_options['fields']['field_tr_menu_item']['table'] = 'field_data_field_tr_menu_item';
  $handler->display->display_options['fields']['field_tr_menu_item']['field'] = 'field_tr_menu_item';
  $handler->display->display_options['fields']['field_tr_menu_item']['label'] = '所属菜单项';
  $handler->display->display_options['fields']['field_tr_menu_item']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_tr_menu_item']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_tr_menu_item']['separator'] = '——';
  /* 字段: 内容: 别名 */
  $handler->display->display_options['fields']['field_alias']['id'] = 'field_alias';
  $handler->display->display_options['fields']['field_alias']['table'] = 'field_data_field_alias';
  $handler->display->display_options['fields']['field_alias']['field'] = 'field_alias';
  /* 字段: 内容: 艺名 */
  $handler->display->display_options['fields']['field_stage_name']['id'] = 'field_stage_name';
  $handler->display->display_options['fields']['field_stage_name']['table'] = 'field_data_field_stage_name';
  $handler->display->display_options['fields']['field_stage_name']['field'] = 'field_stage_name';
  /* 字段: 内容: 相关人物 */
  $handler->display->display_options['fields']['field_er_people']['id'] = 'field_er_people';
  $handler->display->display_options['fields']['field_er_people']['table'] = 'field_data_field_er_people';
  $handler->display->display_options['fields']['field_er_people']['field'] = 'field_er_people';
  $handler->display->display_options['fields']['field_er_people']['settings'] = array(
    'link' => 1,
  );
  $handler->display->display_options['fields']['field_er_people']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_er_people']['separator'] = ' ';
  /* 字段: 字段: 内容类别 */
  $handler->display->display_options['fields']['field_tr_resource_category']['id'] = 'field_tr_resource_category';
  $handler->display->display_options['fields']['field_tr_resource_category']['table'] = 'field_data_field_tr_resource_category';
  $handler->display->display_options['fields']['field_tr_resource_category']['field'] = 'field_tr_resource_category';
  $handler->display->display_options['fields']['field_tr_resource_category']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_tr_resource_category']['delta_offset'] = '0';
  /* 字段: 内容: 地区 */
  $handler->display->display_options['fields']['field_area']['id'] = 'field_area';
  $handler->display->display_options['fields']['field_area']['table'] = 'field_data_field_area';
  $handler->display->display_options['fields']['field_area']['field'] = 'field_area';
  /* 字段: 内容: 日期 */
  $handler->display->display_options['fields']['field_date']['id'] = 'field_date';
  $handler->display->display_options['fields']['field_date']['table'] = 'field_data_field_date';
  $handler->display->display_options['fields']['field_date']['field'] = 'field_date';
  $handler->display->display_options['fields']['field_date']['type'] = 'date_plain';
  $handler->display->display_options['fields']['field_date']['settings'] = array(
    'format_type' => 'y_m_d',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* 字段: 内容: 语种 */
  $handler->display->display_options['fields']['field_tr_language']['id'] = 'field_tr_language';
  $handler->display->display_options['fields']['field_tr_language']['table'] = 'field_data_field_tr_language';
  $handler->display->display_options['fields']['field_tr_language']['field'] = 'field_tr_language';
  $handler->display->display_options['fields']['field_tr_language']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_tr_language']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_tr_language']['separator'] = ' ';
  /* 字段: 内容: 出处 */
  $handler->display->display_options['fields']['field_provenance']['id'] = 'field_provenance';
  $handler->display->display_options['fields']['field_provenance']['table'] = 'field_data_field_provenance';
  $handler->display->display_options['fields']['field_provenance']['field'] = 'field_provenance';
  /* 字段: 内容: 主题 */
  $handler->display->display_options['fields']['field_tags']['id'] = 'field_tags';
  $handler->display->display_options['fields']['field_tags']['table'] = 'field_data_field_tags';
  $handler->display->display_options['fields']['field_tags']['field'] = 'field_tags';
  $handler->display->display_options['fields']['field_tags']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_tags']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_tags']['separator'] = ' ';
  /* 字段: 内容: 描述 */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  /* 字段: 内容: 职责分类 */
  $handler->display->display_options['fields']['field_tr_duty_type']['id'] = 'field_tr_duty_type';
  $handler->display->display_options['fields']['field_tr_duty_type']['table'] = 'field_data_field_tr_duty_type';
  $handler->display->display_options['fields']['field_tr_duty_type']['field'] = 'field_tr_duty_type';
  $handler->display->display_options['fields']['field_tr_duty_type']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_tr_duty_type']['delta_offset'] = '0';
  /* 字段: 内容: 出生日期 */
  $handler->display->display_options['fields']['field_date_of_birth']['id'] = 'field_date_of_birth';
  $handler->display->display_options['fields']['field_date_of_birth']['table'] = 'field_data_field_date_of_birth';
  $handler->display->display_options['fields']['field_date_of_birth']['field'] = 'field_date_of_birth';
  /* 字段: 内容: 死亡日期 */
  $handler->display->display_options['fields']['field_date_of_death']['id'] = 'field_date_of_death';
  $handler->display->display_options['fields']['field_date_of_death']['table'] = 'field_data_field_date_of_death';
  $handler->display->display_options['fields']['field_date_of_death']['field'] = 'field_date_of_death';
  /* 字段: 内容: 性别 */
  $handler->display->display_options['fields']['field_gender']['id'] = 'field_gender';
  $handler->display->display_options['fields']['field_gender']['table'] = 'field_data_field_gender';
  $handler->display->display_options['fields']['field_gender']['field'] = 'field_gender';
  /* 字段: 内容: 籍贯 */
  $handler->display->display_options['fields']['field_ancestry']['id'] = 'field_ancestry';
  $handler->display->display_options['fields']['field_ancestry']['table'] = 'field_data_field_ancestry';
  $handler->display->display_options['fields']['field_ancestry']['field'] = 'field_ancestry';
  /* 字段: 内容: 出生地 */
  $handler->display->display_options['fields']['field_place_of_birth']['id'] = 'field_place_of_birth';
  $handler->display->display_options['fields']['field_place_of_birth']['table'] = 'field_data_field_place_of_birth';
  $handler->display->display_options['fields']['field_place_of_birth']['field'] = 'field_place_of_birth';
  /* 字段: 内容: 民族 */
  $handler->display->display_options['fields']['field_nationality']['id'] = 'field_nationality';
  $handler->display->display_options['fields']['field_nationality']['table'] = 'field_data_field_nationality';
  $handler->display->display_options['fields']['field_nationality']['field'] = 'field_nationality';
  /* 字段: 内容: 家庭住址 */
  $handler->display->display_options['fields']['field_home_address']['id'] = 'field_home_address';
  $handler->display->display_options['fields']['field_home_address']['table'] = 'field_data_field_home_address';
  $handler->display->display_options['fields']['field_home_address']['field'] = 'field_home_address';
  /* 字段: 内容: 供职单位 */
  $handler->display->display_options['fields']['field_institution']['id'] = 'field_institution';
  $handler->display->display_options['fields']['field_institution']['table'] = 'field_data_field_institution';
  $handler->display->display_options['fields']['field_institution']['field'] = 'field_institution';
  /* 字段: 内容: 艺人基本信息 */
  $handler->display->display_options['fields']['field_basic_info']['id'] = 'field_basic_info';
  $handler->display->display_options['fields']['field_basic_info']['table'] = 'field_data_field_basic_info';
  $handler->display->display_options['fields']['field_basic_info']['field'] = 'field_basic_info';
  /* 字段: 内容: 学艺经历 */
  $handler->display->display_options['fields']['field_experience']['id'] = 'field_experience';
  $handler->display->display_options['fields']['field_experience']['table'] = 'field_data_field_experience';
  $handler->display->display_options['fields']['field_experience']['field'] = 'field_experience';
  /* 字段: 内容: 班社和剧团 */
  $handler->display->display_options['fields']['field_troupe']['id'] = 'field_troupe';
  $handler->display->display_options['fields']['field_troupe']['table'] = 'field_data_field_troupe';
  $handler->display->display_options['fields']['field_troupe']['field'] = 'field_troupe';
  /* 字段: 内容: 代表剧目 */
  $handler->display->display_options['fields']['field_typical_work']['id'] = 'field_typical_work';
  $handler->display->display_options['fields']['field_typical_work']['table'] = 'field_data_field_typical_work';
  $handler->display->display_options['fields']['field_typical_work']['field'] = 'field_typical_work';
  /* 字段: 内容: 艺术特点 */
  $handler->display->display_options['fields']['field_artistic_characteristic']['id'] = 'field_artistic_characteristic';
  $handler->display->display_options['fields']['field_artistic_characteristic']['table'] = 'field_data_field_artistic_characteristic';
  $handler->display->display_options['fields']['field_artistic_characteristic']['field'] = 'field_artistic_characteristic';
  /* 字段: 内容: 成就和影响 */
  $handler->display->display_options['fields']['field_achievement']['id'] = 'field_achievement';
  $handler->display->display_options['fields']['field_achievement']['table'] = 'field_data_field_achievement';
  $handler->display->display_options['fields']['field_achievement']['field'] = 'field_achievement';
  /* 字段: 内容: 传承情况 */
  $handler->display->display_options['fields']['field_heritage']['id'] = 'field_heritage';
  $handler->display->display_options['fields']['field_heritage']['table'] = 'field_data_field_heritage';
  $handler->display->display_options['fields']['field_heritage']['field'] = 'field_heritage';
  /* 字段: 内容: 文件 */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  /* 字段: 内容: 文件 */
  $handler->display->display_options['fields']['field_file']['id'] = 'field_file';
  $handler->display->display_options['fields']['field_file']['table'] = 'field_data_field_file';
  $handler->display->display_options['fields']['field_file']['field'] = 'field_file';
  $handler->display->display_options['fields']['field_file']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_file']['type'] = 'file_table';
  /* 字段: 内容: 版权所有者 */
  $handler->display->display_options['fields']['field_copyright_owner']['id'] = 'field_copyright_owner';
  $handler->display->display_options['fields']['field_copyright_owner']['table'] = 'field_data_field_copyright_owner';
  $handler->display->display_options['fields']['field_copyright_owner']['field'] = 'field_copyright_owner';
  /* 字段: 内容: 版权说明 */
  $handler->display->display_options['fields']['field_copyright']['id'] = 'field_copyright';
  $handler->display->display_options['fields']['field_copyright']['table'] = 'field_data_field_copyright';
  $handler->display->display_options['fields']['field_copyright']['field'] = 'field_copyright';
  /* 字段: 内容: 责任方式 */
  $handler->display->display_options['fields']['field_tr_liability_manner_audio']['id'] = 'field_tr_liability_manner_audio';
  $handler->display->display_options['fields']['field_tr_liability_manner_audio']['table'] = 'field_data_field_tr_liability_manner_audio';
  $handler->display->display_options['fields']['field_tr_liability_manner_audio']['field'] = 'field_tr_liability_manner_audio';
  $handler->display->display_options['fields']['field_tr_liability_manner_audio']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_tr_liability_manner_audio']['delta_offset'] = '0';
  /* 字段: 内容: 责任方式 */
  $handler->display->display_options['fields']['field_tr_liability_manner_video']['id'] = 'field_tr_liability_manner_video';
  $handler->display->display_options['fields']['field_tr_liability_manner_video']['table'] = 'field_data_field_tr_liability_manner_video';
  $handler->display->display_options['fields']['field_tr_liability_manner_video']['field'] = 'field_tr_liability_manner_video';
  $handler->display->display_options['fields']['field_tr_liability_manner_video']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_tr_liability_manner_video']['delta_offset'] = '0';
  /* 字段: 内容: 责任方式 */
  $handler->display->display_options['fields']['field_tr_liability_manner_pic']['id'] = 'field_tr_liability_manner_pic';
  $handler->display->display_options['fields']['field_tr_liability_manner_pic']['table'] = 'field_data_field_tr_liability_manner_pic';
  $handler->display->display_options['fields']['field_tr_liability_manner_pic']['field'] = 'field_tr_liability_manner_pic';
  $handler->display->display_options['fields']['field_tr_liability_manner_pic']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_tr_liability_manner_pic']['delta_offset'] = '0';
  /* 字段: 内容: 责任方式 */
  $handler->display->display_options['fields']['field_tr_liability_manner_articl']['id'] = 'field_tr_liability_manner_articl';
  $handler->display->display_options['fields']['field_tr_liability_manner_articl']['table'] = 'field_data_field_tr_liability_manner_articl';
  $handler->display->display_options['fields']['field_tr_liability_manner_articl']['field'] = 'field_tr_liability_manner_articl';
  $handler->display->display_options['fields']['field_tr_liability_manner_articl']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_tr_liability_manner_articl']['delta_offset'] = '0';
  /* 字段: 内容: 责任者 */
  $handler->display->display_options['fields']['field_resp_by']['id'] = 'field_resp_by';
  $handler->display->display_options['fields']['field_resp_by']['table'] = 'field_data_field_resp_by';
  $handler->display->display_options['fields']['field_resp_by']['field'] = 'field_resp_by';
  /* 语境过滤器: 内容: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'feed_nid';
  $handler->display->display_options['arguments']['nid']['exception']['title'] = '全部';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';

  /* Display: 页面 */
  $handler = $view->new_display('page', '页面', 'page_1');
  $handler->display->display_options['path'] = 'node/%/imported';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = '数据';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['imported_items'] = array(
    t('Master'),
    t('更多'),
    t('应用'),
    t('重置'),
    t('排序依据'),
    t('升序'),
    t('降序'),
    t('每页条目数'),
    t('- 全部 -'),
    t('偏移量'),
    t('« 第一页'),
    t('‹ 前一页'),
    t('下一页 ›'),
    t('末页 »'),
    t('Feed owner'),
    t('类型'),
    t('编辑'),
    t('艺人照片'),
    t('名称'),
    t('所属菜单项'),
    t('别名'),
    t('艺名'),
    t('相关人物'),
    t('内容类别'),
    t('地区'),
    t('日期'),
    t('语种'),
    t('出处'),
    t('主题'),
    t('描述'),
    t('职责分类'),
    t('出生日期'),
    t('死亡日期'),
    t('性别'),
    t('籍贯'),
    t('出生地'),
    t('民族'),
    t('家庭住址'),
    t('供职单位'),
    t('艺人基本信息'),
    t('学艺经历'),
    t('班社和剧团'),
    t('代表剧目'),
    t('艺术特点'),
    t('成就和影响'),
    t('传承情况'),
    t('文件'),
    t('版权所有者'),
    t('版权说明'),
    t('责任方式'),
    t('责任者'),
    t('全部'),
    t('页面'),
  );
  $export['imported_items'] = $view;

  return $export;
}
