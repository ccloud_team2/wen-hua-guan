<?php
/**
 * @file
 * whg_data_import.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function whg_data_import_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'data_import_audio';
  $feeds_importer->config = array(
    'name' => 'Data import - audio',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 1,
        'directory' => 'private://import',
        'allowed_schemes' => array(
          'public' => 'public',
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => '名称',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => '所属菜单项',
            'target' => 'field_tr_menu_item',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => '相关人物',
            'target' => 'field_er_people:etid',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => '内容类别',
            'target' => 'field_tr_resource_category',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => '地区',
            'target' => 'field_area',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => '日期',
            'target' => 'field_date:start',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => '语种',
            'target' => 'field_tr_language',
            'term_search' => '0',
            'autocreate' => 1,
          ),
          7 => array(
            'source' => '出处',
            'target' => 'field_provenance',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => '主题',
            'target' => 'field_tags',
            'term_search' => '0',
            'autocreate' => 1,
          ),
          9 => array(
            'source' => '描述',
            'target' => 'body',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => '文件路径',
            'target' => 'field_file:uri',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'conent_edit',
        'skip_hash_check' => 0,
        'bundle' => 'audio',
      ),
    ),
    'content_type' => 'audio_import',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['data_import_audio'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'data_import_person';
  $feeds_importer->config = array(
    'name' => 'Data import - person',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 1,
        'directory' => 'private://import',
        'allowed_schemes' => array(
          'public' => 'public',
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => '姓名',
            'target' => 'title',
            'unique' => 1,
          ),
          1 => array(
            'source' => '别名',
            'target' => 'field_alias',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => '艺名',
            'target' => 'field_stage_name',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => '主题',
            'target' => 'field_tags',
            'term_search' => '0',
            'autocreate' => 1,
          ),
          4 => array(
            'source' => '职责分类',
            'target' => 'field_tr_duty_type',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => '内容类别',
            'target' => 'field_tr_resource_category',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => '出生日期',
            'target' => 'field_date_of_birth',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => '死亡日期',
            'target' => 'field_date_of_death',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => '性别',
            'target' => 'field_gender',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => '籍贯',
            'target' => 'field_ancestry',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => '出生地',
            'target' => 'field_place_of_birth',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => '民族',
            'target' => 'field_nationality',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => '家庭住址',
            'target' => 'field_home_address',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => '供职单位',
            'target' => 'field_institution',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => '艺人基本信息',
            'target' => 'field_basic_info',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => '学艺经历',
            'target' => 'field_experience',
            'unique' => FALSE,
          ),
          16 => array(
            'source' => '班社和剧团',
            'target' => 'field_troupe',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => '代表剧目',
            'target' => 'field_typical_work',
            'unique' => FALSE,
          ),
          18 => array(
            'source' => '艺术特点',
            'target' => 'field_artistic_characteristic',
            'unique' => FALSE,
          ),
          19 => array(
            'source' => '成就和影响',
            'target' => 'field_achievement',
            'unique' => FALSE,
          ),
          20 => array(
            'source' => '传承情况',
            'target' => 'field_heritage',
            'unique' => FALSE,
          ),
          21 => array(
            'source' => '文件地址',
            'target' => 'field_photo:uri',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'conent_edit',
        'skip_hash_check' => 0,
        'bundle' => 'person',
      ),
    ),
    'content_type' => 'person_import',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['data_import_person'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'data_import_picture';
  $feeds_importer->config = array(
    'name' => 'Data import - picture',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 1,
        'directory' => 'private://import',
        'allowed_schemes' => array(
          'public' => 'public',
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => '名称',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => '内容类别',
            'target' => 'field_tr_resource_category',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          2 => array(
            'source' => '主题',
            'target' => 'field_tags',
            'term_search' => '0',
            'autocreate' => 1,
          ),
          3 => array(
            'source' => '描述',
            'target' => 'body',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => '地区',
            'target' => 'field_area',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => '出处',
            'target' => 'field_provenance',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => '日期',
            'target' => 'field_date:start',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => '相关人物',
            'target' => 'field_er_people:etid',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => '所属菜单项',
            'target' => 'field_tr_menu_item',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => '文件路径',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'conent_edit',
        'skip_hash_check' => 0,
        'bundle' => 'picture',
      ),
    ),
    'content_type' => 'picture_import',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['data_import_picture'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'data_import_video';
  $feeds_importer->config = array(
    'name' => 'Data import - video',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 1,
        'directory' => 'private://import',
        'allowed_schemes' => array(
          'public' => 'public',
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => '名称',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => '所属菜单项',
            'target' => 'field_tr_menu_item',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => '相关人物',
            'target' => 'field_er_people:etid',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => '内容类别',
            'target' => 'field_tr_resource_category',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => '地区',
            'target' => 'field_area',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => '日期',
            'target' => 'field_date:start',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => '语种',
            'target' => 'field_tr_language',
            'term_search' => '0',
            'autocreate' => 1,
          ),
          7 => array(
            'source' => '出处',
            'target' => 'field_provenance',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => '主题',
            'target' => 'field_tags',
            'term_search' => '0',
            'autocreate' => 1,
          ),
          9 => array(
            'source' => '描述',
            'target' => 'body',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => '文件路径',
            'target' => 'field_file:uri',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'conent_edit',
        'skip_hash_check' => 0,
        'bundle' => 'video',
      ),
    ),
    'content_type' => 'video_import',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['data_import_video'] = $feeds_importer;

  return $export;
}
