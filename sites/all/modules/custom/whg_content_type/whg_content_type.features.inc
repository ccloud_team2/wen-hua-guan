<?php
/**
 * @file
 * whg_content_type.features.inc
 */

/**
 * Implements hook_node_info().
 */
function whg_content_type_node_info() {
  $items = array(
    'article' => array(
      'name' => t('文章'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'audio' => array(
      'name' => t('音频'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'message' => array(
      'name' => t('读者留言'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('姓名'),
      'help' => '',
    ),
    'person' => array(
      'name' => t('人物'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('姓名'),
      'help' => '',
    ),
    'picture' => array(
      'name' => t('图片'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'video' => array(
      'name' => t('视频'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
