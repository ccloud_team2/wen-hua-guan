<?php
/**
 * @file
 * whg_data_export.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function whg_data_export_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-views-export.
  $menus['menu-views-export'] = array(
    'menu_name' => 'menu-views-export',
    'title' => '内容导出',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('内容导出');


  return $menus;
}
