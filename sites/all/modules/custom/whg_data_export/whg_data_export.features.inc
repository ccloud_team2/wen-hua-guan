<?php
/**
 * @file
 * whg_data_export.features.inc
 */

/**
 * Implements hook_views_api().
 */
function whg_data_export_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
