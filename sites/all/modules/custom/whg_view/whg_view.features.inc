<?php
/**
 * @file
 * whg_view.features.inc
 */

/**
 * Implements hook_views_api().
 */
function whg_view_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
